import "@mui/material/Paper";
import "@mui/material/Button";
import "@mui/material/Typography";
import "@mui/material/Avatar";
import { Mixins as muiMixins, MixinsOptions as muiMixinsOptions } from '@mui/material/styles';
import { 
  PaletteOptions as muiPaletteOptions, 
  Palette as muiPalette, 
  PaletteColor as muiPaletteColor,
  SimplePaletteColorOptions as muiSimplePaletteColorOptions
} from '@mui/material/styles'

declare module "@mui/material/Paper" {
  interface PaperPropsVariantOverrides {
    white: true;
    primary: true;
    grey: true;
    red: true;
    orange: true;
    pink: true;
    brown: true;
    blue: true;
    lightBlue: true;
    purple: true;
    green: true;
    iron: true;
    accountSetting: true;
    springGreen: true;
    electricBlue: true;
    goldenrod: true;
    smallLightGrey: true;
    smallPrimary: true;
  }
}
declare module "@mui/material/Button" {
  interface ButtonPropsColorOverrides {
    greenLight?: true;
  }
  interface ButtonPropsVariantOverrides {
    grad: true;
    dark: true;
    light: true;
    grey: true;
    textLightGrey?: true;
    textDarkGrey?: true;
    white?: true;
    whiteDisabled?: true
  }
}
declare module "@mui/material/Typography" {
  interface TypographyPropsVariantOverrides {
    grey: true;
    dark: true;
    darkB: true;
    headerDashboard: true;
  }
}
declare module "@mui/material/Avatar" {
  interface AvatarPropsVariantOverrides {
    grey: true;
    greyB: true;
  }
}
declare module "@mui/material/Chip" {
  interface ChipPropsVariantOverrides {
    completed: true;
  }
}


declare module '@mui/material/styles' {

  interface Palette extends muiPalette {
    springGreen: React.CSSProperties["color"];
    springGreenOP: React.CSSProperties["color"];
    electricBlue: React.CSSProperties["color"];
    electricBlueOP: React.CSSProperties["color"];
    goldenrod: React.CSSProperties["color"];
    goldenrodOP: React.CSSProperties["color"];
    brown: React.CSSProperties["color"];
    brownOP: React.CSSProperties["color"];
    pink: React.CSSProperties["color"];
    pinkOP: React.CSSProperties["color"];
    blue: React.CSSProperties["color"];
    blueOP: React.CSSProperties["color"];
    lightBlue: React.CSSProperties["color"];
    lightBlueOP: React.CSSProperties["color"];
    purple: React.CSSProperties["color"];
    purpleOP: React.CSSProperties["color"];
    green: React.CSSProperties["color"];
    greenOP: React.CSSProperties["color"];
    iron: React.CSSProperties["color"];
    ironOP: React.CSSProperties["color"];
    common: {
      black: string;
      white: string;
    };
  }

  interface PaletteOptions extends muiPaletteOptions {
    springGreen: React.CSSProperties["color"];
    springGreenOP: React.CSSProperties["color"];
    electricBlue: React.CSSProperties["color"];
    electricBlueOP: React.CSSProperties["color"];
    goldenrod: React.CSSProperties["color"];
    goldenrodOP: React.CSSProperties["color"];
    brown: React.CSSProperties["color"];
    brownOP: React.CSSProperties["color"];
    pink: React.CSSProperties["color"];
    pinkOP: React.CSSProperties["color"];
    blue: React.CSSProperties["color"];
    blueOP: React.CSSProperties["color"];
    lightBlue: React.CSSProperties["color"];
    lightBlueOP: React.CSSProperties["color"];
    purple: React.CSSProperties["color"];
    purpleOP: React.CSSProperties["color"];
    green: React.CSSProperties["color"];
    greenOP: React.CSSProperties["color"];
    iron: React.CSSProperties["color"];
    ironOP: React.CSSProperties["color"];

    common: {
      black: string;
      white: string;
    };
  }

  interface PaletteColor extends muiPaletteColor {
    opacity?: string;
  }

  interface SimplePaletteColorOptions extends muiSimplePaletteColorOptions{
    opacity?: string;
  }

  interface  Mixins extends muiMixins {
    flexCenter: React.CSSProperties,
    greyWhiteBtn?: (state) => React.CSSProperties,
    borderBox: (width: number, color: string) => React.CSSProperties,
    toolsCard: (bgColor: string, color: string) => React.CSSProperties,
    toolbar?: React.CSSProperties,
  }

  interface  MixinsOptions extends muiMixinsOptions {
    flexCenter: React.CSSProperties,
    greyWhiteBtn?: (state) => React.CSSProperties,
    borderBox: (width: number, color: string) => React.CSSProperties,
    toolsCard: (bgColor: string, color: string) => React.CSSProperties,
    toolbar?: React.CSSProperties,
  }

}