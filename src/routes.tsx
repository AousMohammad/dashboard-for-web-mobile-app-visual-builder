import { PrivateRoute } from "./components";
import { Route } from "./types";
import {
  Home as HomeIcon,
  Orders,
  Categories,
  Products,
  Customers,
  Analytics,
  Payment,
  Vouchers,
  Settings,
  DeliveryPickup,
  Integrations
} from './icons';
import { Home, AccountSetting, HomeToolsPage, HomeEditProfilePage, HomeEditProfileChangePasswordPage, Colors, TypographyPage, Design, OrderPage } from "./pages";
import { FontPage } from "pages/design/views/font/container";
import { Dashboard } from "pages/dashboard/views/home/container/Dashboard";
import { DashboardOutlined } from "@mui/icons-material";



const routes: Route[] = [
  {
    path: '/',
    element: <PrivateRoute />,
    children: [
      {
        path: 'dashboard',
        element: <Dashboard />,
        name: 'dashboard',
        icon: <DashboardOutlined width="20px" height="22px" />,
      },
      {
        path: 'home',
        name: 'home',
        icon: <HomeIcon width="20px" height="22px" />,
        children: [{
          path: "tools",
          element: <HomeToolsPage />,
          name: "tools"
        }, {
          path: "edit-profile",
          element: <HomeEditProfilePage />,
          name: "edit-profile",
        }, {
            path: "change-password",
            element: <HomeEditProfileChangePasswordPage />,
            name: "change password"
          },
        ]
      },
      {
        path: 'orders',
        element: <OrderPage />,
        name: 'orders',
        icon: <Orders width="20px" height="22px" />
      },
      {
        path: 'categories',
        element: <div>Categories</div>,
        name: 'categories',
        icon: <Categories width="20px" height="22px" />
      },
      {
        path: 'products',
        element: <div>Products</div>,
        name: 'products',
        icon: <Products width="20px" height="22px" />
      },
      {
        path: 'customers',
        element: <div>Customers</div>,
        name: 'customers',
        icon: <Customers width="20px" height="22px" />
      },
      {
        path: 'analytics',
        element: <div>Analytics</div>,
        name: 'analytics',
        icon: <Analytics width="20px" height="22px" />
      },
      {
        path: 'payment-methods',
        element: <div>Payment Methods</div>,
        name: 'payment_methods',
        icon: <Payment width="20px" height="22px" />
      },
      {
        path: 'vouchers',
        element: <div>Vouchers</div>,
        name: 'vouchers',
        icon: <Vouchers width="20px" height="22px" />
      },
      {
        path: 'account-settings',
        element: <AccountSetting />,
        name: 'account_settings',
        icon: <Settings width="20px" height="22px" />
      },
      {
        path: 'delivery-pickup',
        element: <div>Delivery & Pickup</div>,
        name: 'delivery_pickup',
        icon: <DeliveryPickup width="20px" height="22px" />
      },
      {
        path: 'integrations',
        element: <div>Integrations</div>,
        name: 'integrations',
        icon: <Integrations width="20px" height="22px" />
      },
      {
        path: 'colors',
        element: <Colors />,
        name: 'colors',
        icon: <>s</>
      },
      {
        path: 'typography',
        element: <TypographyPage />,
        name: 'typography',
        icon: <>s</>
      },
      {
        path: 'design',
        element: <Design />,
        name: 'design',
        icon: <>s</>,
        children: [{
          path: 'font',
          element: <FontPage />,
          name: 'font',
          icon: <>s</>,
        }]
      },
    ]
  }
  // Add other routes here that should be protected
];

export default routes;
