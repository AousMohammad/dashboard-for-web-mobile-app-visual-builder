import * as React from 'react';
import { CssBaseline, Box, Toolbar, List, Container, ListItem, ListItemButton, ListItemIcon, ListItemText, useTheme, useMediaQuery, Theme } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { getDirection } from 'utils/getDirection';
import routes from 'routes';
import { Drawer } from "components/layout/Layout"
import { DashboardTopNav } from '../components';
import { Route } from 'types/index';
import _ from 'lodash';
import { useLocation, useNavigate } from 'react-router-dom';
import { BreadCrumb } from 'components/BreadCrumb';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';

interface LayoutProps {
    children?: React.ReactNode;
}




export const Layout: React.FC<LayoutProps> = ({ children }) => {
    const [activeRoute, setActiveRoute] = React.useState("home");
    const theme = useTheme();
    const location = useLocation();
    const { t } = useTranslation();
    const direction = getDirection();
    const navigate = useNavigate();
    const onDrawerItemClick = (path: string, parentPath?: string) => {
        const fullPath = parentPath ? `${parentPath}/${path}` : path;
        setActiveRoute(getPath(fullPath));
        navigate(fullPath);
      };
    const getPath = (path: string) => {
        const parts = path.split('/');
        return parts.join('/');
    };
    const matches = useMediaQuery(theme.breakpoints.up('md'))
    const [open, setOpen] = React.useState<Record<string, boolean>>({});

    React.useEffect(() => {
        setOpen(prevOpen => ({ ...prevOpen, [activeRoute]: matches }));
    }, [matches, activeRoute]);

    const handleClick = (path: string) => {
        setOpen((prevOpen: any) => ({
            ...prevOpen,
            [path]: !prevOpen[path],
        }));
    };

    React.useEffect(() => {
        setActiveRoute(getPath(location.pathname));
    }, [location]);

    return (
        <Box sx={{
            display: 'flex', '& header': {
                marginLeft: direction === 'ltr' ? '240px' : '0 !important',
                left: direction === 'ltr' ? 'auto' : '0 !important',
                right: direction === 'ltr' ? '0' : 'auto !important',
                backgroundColor: 'transparent',
            }
        }}>
            <CssBaseline />
            <DashboardTopNav />
            <Drawer variant={!matches ? 'temporary' : 'permanent'} open={open[activeRoute]} sx={{ width: '12vw' }}>
                <List sx={{ padding: matches ? "10px" : "0px" }} component="nav" style={{ marginTop: '7vh' }}>
                    {routes[0].children?.map((route) => (
                        <React.Fragment key={_.uniqueId()}>
                            <ListItem
                                button
                                onClick={() => {
                                    if (route.children) {
                                        handleClick(route.path);
                                    } else {
                                        onDrawerItemClick(route.path);
                                    }
                                }}
                                className={activeRoute === route.path ? "active" : ""}
                                sx={{
                                    borderRadius: activeRoute === route.path ? "12px" : "0px",
                                }}
                                key={t(route.name!)}
                                disablePadding
                            >
                                <ListItemButton sx={{ "& .MuiListItemIcon-root": { minWidth: "45px" } }}>
                                    <ListItemIcon>
                                        {route.icon}
                                    </ListItemIcon>
                                    <ListItemText
                                        sx={{
                                            color: activeRoute === route.path ? theme.palette.secondary.main : theme.palette.grey['A700'],
                                            "& span": {
                                                fontWeight: activeRoute === route.path ? "bold" : "normal",
                                            }
                                        }}
                                        primary={t(route.name!)} />
                                    {route.children && (open[route.path] ? <ExpandLess /> : <ExpandMore />)}
                                </ListItemButton>
                            </ListItem>
                            {route.children && (
                                <Collapse in={open[route.path]} timeout="auto" unmountOnExit>
                                    <List component="div" disablePadding>
                                        {route.children.map((childRoute) => (
                                            <ListItem
                                                key={_.uniqueId()}
                                                button
                                                className={activeRoute === `${route.path}/${childRoute.path}` ? 'active' : ''}
                                                onClick={() => onDrawerItemClick(childRoute.path, route.path)}
                                            >
                                                <ListItemIcon>{childRoute.icon}</ListItemIcon>
                                                <ListItemText primary={t(childRoute.name!)} />
                                            </ListItem>
                                        ))}
                                    </List>
                                </Collapse>
                            )}
                        </React.Fragment>
                    ))}
                </List>
            </Drawer>
            <Box
                component="main"
                sx={{
                    backgroundColor: (theme) =>
                        theme.palette.mode === 'light'
                            ? theme.palette.grey[100]
                            : theme.palette.grey[900],
                    flexGrow: 1,
                    height: '100vh',
                    overflow: 'auto',
                }}
            >
                <Toolbar />
                <Container maxWidth="xl" sx={{ mt: 4, mb: 4 }}>
                    <BreadCrumb />
                    {children}
                </Container>
            </Box>
        </Box>
    );
}