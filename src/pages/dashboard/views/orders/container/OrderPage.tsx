import { Box, Button, Grid, Typography } from '@mui/material'
import React from 'react'
import BarChartIcon from '@mui/icons-material/BarChart';
import AddIcon from '@mui/icons-material/Add';
import { ChevronRight } from '@mui/icons-material';
import { ButtonGroupWhite } from 'components/buttons';

export const OrderPage: React.FC = () => {
    return (
        <Grid container xs={12} spacing={1}>
            <Grid item xs={8}>
                <Box width="100%" display="flex" justifyContent="space-between" alignItems="center">
                    {/* <Typography component="h1">Orders</Typography> */}
                    <Box display="flex" flexDirection="column" alignItems="center" columnGap={1}>
                        <Button color="secondary" startIcon={<BarChartIcon />}>color secondary</Button>
                        <Button startIcon={<AddIcon fontSize="large" />}>color Primary</Button>
                        <Button color='info'>Color Info</Button>
                        <Button color="error" startIcon={<AddIcon fontSize="large" />}>color error</Button>
                        <Button variant="textLightGrey" endIcon={<ChevronRight fontSize="large" />}>text button</Button>
                        <Button variant="textDarkGrey" endIcon={<AddIcon fontSize="large" />}>Add Section</Button>
                        <Button variant="textDarkGrey" endIcon={<ChevronRight fontSize="large" />}>Home Page</Button>
                        <ButtonGroupWhite btns={[{ title: "style"}, { title: "layout"}]} />
                        <Button  sx={{ flexDirection: "column", width: "118px", height: "100px", borderRadius: "16px", border: "2px solid #1BFCB6", backgroundColor: "#CDFBED"}} color="secondary"><Typography>Mon</Typography><Typography>18</Typography></Button>
                    </Box>
                </Box>
            </Grid>
            <Grid item xs={4}>
                Drawer
            </Grid>
        </Grid>
    )
}