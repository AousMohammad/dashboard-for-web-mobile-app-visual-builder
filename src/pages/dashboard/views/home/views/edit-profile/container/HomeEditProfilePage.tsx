import React from 'react'

import { useTranslation } from 'react-i18next'
import { Grid, Typography } from '@mui/material'
import { HomeEditProfileForm } from '../components'

const defaultValues = {
    firstName: "Ahmed",
    lastName: "Omar",
    country: "syria",
    email: "ahmed.omar@gmail.com",
    password: "string",
}
export const HomeEditProfilePage: React.FC = () => {
    const { t } = useTranslation()
    return (
        <Grid container xs={12} rowGap={2} columnGap={2}>
            <Grid item xs={12}><Typography variant="darkB" fontSize="30px">{t("edit_profile")}</Typography> </Grid>
            <Grid item mb={3} xs={12}><Typography variant="grey" fontSize="16px">{t("edit_profile_subtitle")}</Typography> </Grid>
            <Grid item container lg={4} md={6} xs={12}>
                <HomeEditProfileForm defaultValues={defaultValues} />
            </Grid>
        </Grid>
    )
}