import { InputAdornment, Typography } from '@mui/material';
import CButton from 'components/CButton';
import { UserCradentialFields, UserNameFields } from 'pages/authinication/views/Signup/components';
import CountryComboBox from 'pages/authinication/views/Signup/components/CountryCombo';
import React from 'react'
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom';


export const HomeEditProfileForm: React.FC<THomeEditProfileFormProps> = ({ defaultValues }) => {
    const { register, watch, handleSubmit, formState } = useForm<FormValues>({ defaultValues});
    const { t } = useTranslation();
    const navigate = useNavigate();

    const onSubmit = (data: FormValues) => {
        console.log(data);
        // navigate('/verify', { replace: true })
        // Perform your login action here
    };

    const [firstName, lastName, email, password] = watch(['firstName', 'lastName', 'email', 'password']);

    return (
        <div style={{ display: 'flex', flexDirection: 'column', position: 'relative', alignItems: 'center', width: '100%' }}>
            <form onSubmit={handleSubmit(onSubmit)} style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
                <UserNameFields register={register} formState={formState} />
                <CountryComboBox register={register} formState={formState} />
                <UserCradentialFields
                    register={register}
                    formState={formState}
                    emailTextFieldProps={{ inputProps: {}}}
                    passwordTextFieldProps={{
                        InputProps: {
                            endAdornment: (
                                <InputAdornment position="start">
                                    <Typography onClick={() => navigate("/edit-profile", { state: password})} sx={{ cursor: "pointer"}} variant='grey'>{t("change")}</Typography>
                                </InputAdornment>
                              ),
                        }
                    }}
                    >
                    <CButton disabled={!(firstName && lastName && email && password)} type="submit" borderRadius="lg" color="primary" text={t('save')} />
                </UserCradentialFields>
            </form>
        </div>
    )
}

type THomeEditProfileFormProps = {
    defaultValues?: FormValues
}
export type FormValues = {
    firstName: string;
    lastName: string;
    country: string;
    email: string;
    password: string;
};