import React from 'react'

import { useTranslation } from 'react-i18next'
import { Grid, Typography } from '@mui/material'
import { DashboardNavigationBar } from 'pages/dashboard/views/home/components'
import { useLocation } from 'react-router-dom'
import { HomeEditProfileChangePasswordForm } from '../components'

export const HomeEditProfileChangePasswordPage: React.FC = () => {
    const location = useLocation();
    const { t } = useTranslation()

    const defaultValues = {
        currentPassword: (location.state || "123654") as string
    }
    const navigationBar = [{
        title: t("home"), 
        onClick: () => console.log("home")
    }, { 
        title: t("edit_profile"), 
        onClick: () => console.log("edit_profile")
    }, {
        title: t("change_password"),
        onClick: () => ({})
    }]
    return (
        <Grid container xs={12} rowGap={2} columnGap={2}>
            <Grid item xs={12}> <DashboardNavigationBar navigationBar={navigationBar} /></Grid>
            <Grid item xs={12}><Typography variant="darkB" fontSize="30px">{t("change_password")}</Typography> </Grid>
            <Grid item mb={3} xs={12}><Typography variant="grey" fontSize="16px">{t("change_password_subtitle")}</Typography> </Grid>
            <Grid item container lg={4} md={6} xs={12}>
                <HomeEditProfileChangePasswordForm defaultValues={defaultValues} />
            </Grid>
        </Grid>
    )
}
