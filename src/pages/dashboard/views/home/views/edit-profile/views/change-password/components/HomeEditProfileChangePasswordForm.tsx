import React from 'react'

import CButton from 'components/CButton';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useVisibilityPassword } from 'pages/authinication/hooks';
import { FormControl, InputLabel, TextField } from '@mui/material';

export const HomeEditProfileChangePasswordForm: React.FC<THomeEditProfileChangePasswordFormProps> = ({ defaultValues }) => {
    const { register, watch, handleSubmit, formState: { errors}, setError} = useForm<FormValues>({ defaultValues});
    const { t } = useTranslation();
    const navigate = useNavigate();
    const { passwordType: newPasswordType, VisibilityIcon: NewPasswordVisibilityIcon } = useVisibilityPassword()
    const { passwordType: repPasswordType, VisibilityIcon: RepPasswordVisibilityIcon } = useVisibilityPassword()
    const { passwordType: currPasswordType, VisibilityIcon: CurrPasswordVisibilityIcon } = useVisibilityPassword()

    const onSubmit = (data: FormValues) => {
        if (data.newPassword !== data.confirmPassword) {
            setError("confirmPassword", {message: t("textField_resetPasswordError")});
            setError("newPassword", {message: t("textField_resetPasswordError")});
            return
        }
        console.log(data);
    };
    const [currentPassword, newPassword, confirmPassword] = watch(["currentPassword", "newPassword", "confirmPassword"]);

    return (
        <div style={{ display: 'flex', flexDirection: 'column', position: 'relative', alignItems: 'center', width: '100%' }}>
            <form onSubmit={handleSubmit(onSubmit)} style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
                <FormControl>
                    <InputLabel>{t("current_password")}</InputLabel>
                    <TextField
                        type={currPasswordType}
                        sx={{ mb: 3 }}
                        {...register("currentPassword", { required: t('required_password') })}
                        error={!!errors.currentPassword}
                        helperText={errors.currentPassword?.message}
                        placeholder={t("password_placeholder")}
                        InputProps={{
                            endAdornment: <CurrPasswordVisibilityIcon />
                        }}
                    />
                </FormControl>
                <FormControl>
                    <InputLabel>{t("new_password")}</InputLabel>
                    <TextField
                        type={newPasswordType}
                        sx={{ mb: 3 }}
                        {...register("newPassword", { required: t('required_password') })}
                        error={!!errors.newPassword}
                        helperText={errors.newPassword?.message}
                        placeholder={t("your_new_password")}
                        InputProps={{ endAdornment: <NewPasswordVisibilityIcon /> }}
                    />
                </FormControl>
                <FormControl>
                    <InputLabel>{t("repate_password")}</InputLabel>
                    <TextField
                        type={repPasswordType}
                        sx={{ mb: 3 }}
                        {...register("confirmPassword", { required: t('required_password') })}
                        error={!!errors.confirmPassword}
                        helperText={errors.confirmPassword?.message}
                        placeholder={t("your_new_password")}
                        InputProps={{ endAdornment: <RepPasswordVisibilityIcon /> }}
                    />
                </FormControl>
                <CButton disabled={!(currentPassword && newPassword && confirmPassword)} type="submit" borderRadius="lg" color="primary" text={t('change')} />
            </form>
        </div>
    )
}

type THomeEditProfileChangePasswordFormProps = {
    defaultValues?: FormValues
}
export type FormValues = {
    currentPassword: string;
    newPassword?: string;
    confirmPassword?: string;
};