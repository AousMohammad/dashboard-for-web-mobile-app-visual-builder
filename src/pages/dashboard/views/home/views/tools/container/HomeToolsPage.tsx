import React from 'react'

import { useTranslation } from 'react-i18next'
import { Grid, Typography } from '@mui/material'
import { useRenderToolCards } from '../../../hooks'

export const HomeToolsPage: React.FC = () => {
    const { t } = useTranslation()
    const renerCards = useRenderToolCards()

    return (
        <Grid container xs={12} rowGap={2} columnGap={2} >
            <Grid item xs={12}><Typography variant="darkB" fontSize="30px">{t("Website_Tools")}</Typography> </Grid>
            <Grid item mb={3} xs={12}><Typography variant="grey" fontSize="16px">{t("Website_Tools_subtitle")}</Typography> </Grid>
            {renerCards()}
        </Grid>
    )
}