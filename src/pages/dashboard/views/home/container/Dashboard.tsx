import { Avatar, Box, Button, Card, Grid, Stack, Typography } from '@mui/material'
import { Website } from 'icons'
import { ChartLine, IOSSwitch, KnobProgress, ScrollerBox, SummaryCard } from 'components';
import { Flag, HomeMax } from '@mui/icons-material';
import { OrdersCard } from 'components/OrdersCard/OrdersCard';
import { ProductCard } from 'components/ProductCard';
import { BigProductCard } from 'components/BigProductCard';
import { SummaryCardSkeleton } from 'components/SummaryCard/SummaryCardSkeleton';
import { OrdersCardSkeleton } from 'components/OrdersCard/OrdersCardSkeleton';
import { TaskCard } from 'components/TaskCard/TaskCard';
import BasicTabs from 'components/Tabs';
import TemporaryDrawer from 'components/Drawer';
import HorizontalNonLinearStepper from 'components/Stepper';

type Props = {}

export const Dashboard = (props: Props) => {
    return (
        <Grid container spacing={5} columnSpacing={3}>
            <Grid item xs={12}>
                <Card variant='white'>
                    <Box display="flex" justifyContent="space-around" alignItems="center">
                        <Box flex={1}>
                            <Card variant='iron'>
                                <Website style={{ transform: 'scale(1.3)' }} width='60px' height='40px' />
                            </Card>
                        </Box>
                        <Box flex={7} padding={2}>
                            <Box>
                                <Typography variant='darkB'>Test</Typography>
                            </Box>
                            <Typography variant='grey'>Test</Typography>
                            <div>
                                <IOSSwitch /> <Typography variant='grey'>Test</Typography>
                            </div>
                        </Box>
                        <Box flex={4}>
                            <Stack direction='row' justifyContent='center' alignItems='center'>
                                <Box padding={2} display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
                                    <Avatar variant='greyB'>
                                        <HomeMax />
                                    </Avatar>
                                    <Typography variant='grey'>Test</Typography>
                                </Box>
                                <Box padding={2} display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
                                    <Avatar variant='greyB'>
                                        <HomeMax />
                                    </Avatar>
                                    <Typography variant='grey'>Test</Typography>
                                </Box>
                                <Box padding={2} display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
                                    <Avatar variant='greyB'>
                                        <HomeMax />
                                    </Avatar>
                                    <Typography variant='grey'>Test</Typography>
                                </Box>
                                <Box padding={2} display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
                                    <Avatar variant='greyB'>
                                        <HomeMax />
                                    </Avatar>
                                    <Typography variant='grey'>Test</Typography>
                                </Box>
                                <Box padding={2} display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
                                    <Avatar variant='greyB'>
                                        <HomeMax />
                                    </Avatar>
                                    <Typography variant='grey'>Test</Typography>
                                </Box>
                            </Stack>
                        </Box>
                    </Box>
                </Card>
            </Grid>
            <Grid item xs={12} md={6}>
                <Card variant='green'>
                    <KnobProgress />
                    <Button variant='dark' style={{ width: '140px' }}>
                        ss
                    </Button>
                    <Button variant='light'>
                        ss
                    </Button>
                </Card>
            </Grid>
            <Grid item xs={12} md={6}>
                <Card variant='white' style={{ height: '100%' }}>
                    <Box>
                        <Typography variant='darkB'>Want Custom Edits?</Typography>
                        <Typography variant='grey'>You can renew your subscription.</Typography>
                    </Box>
                    <Box flexDirection='row' display="flex">
                        <Button variant='grad'>
                            <Flag />
                            Over Zaki
                        </Button>
                        <Button variant='grey'>
                            ss
                        </Button>
                    </Box>
                </Card>
            </Grid>
            <Grid item xs={12} md={12}>
                <Card variant='white' style={{ flexDirection: 'column' }}>
                    <ChartLine />
                </Card>
            </Grid>
            <Grid item xs={2} md={2}>
                <Card variant='red' style={{ flexDirection: 'column' }}>
                    <Box>
                        <Typography variant='darkB'>Want Custom Edits?</Typography>
                        <Typography variant='grey'>You can renew your subscription.</Typography>
                    </Box>
                    <Box flexDirection='row' display="flex">
                        <Button variant='grad'>
                            ss
                        </Button>
                        <Button variant='grey'>
                            ss
                        </Button>
                    </Box>
                </Card>
            </Grid>
            <Grid item xs={12} md={12}>
                <Card variant='orange' style={{ flexDirection: 'column' }}>
                    <Box>
                        <Typography variant='darkB'>Want Custom Edits?</Typography>
                        <Typography variant='grey'>You can renew your subscription.</Typography>
                    </Box>
                    <Box flexDirection='row' display="flex">
                        <Button variant='grad'>
                            ss
                        </Button>
                        <Button variant='grey'>
                            ss
                        </Button>
                    </Box>
                </Card>
            </Grid>
            <Grid item xs={12} md={12}>
                <ScrollerBox>
                    <Card variant='brown' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='pink' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='brown' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='pink' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='brown' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='pink' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='brown' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='pink' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='brown' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='pink' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='brown' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='pink' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='brown' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='pink' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='brown' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='pink' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='brown' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                    <Card variant='pink' style={{ flexDirection: 'column', height: '160px', width: '160px', justifyContent: 'space-between' }}>
                        <Box>
                            <Typography variant='darkB'>Want</Typography>
                        </Box>
                        <Box flexDirection='row' display="flex">
                            ss
                        </Box>
                    </Card>
                </ScrollerBox>
            </Grid>
            <Grid item xs={12}>
                <Card variant='white' style={{ height: '200px', width: '100%' }}>
                    &lt;Card variant='white' /&gt;
                </Card>
            </Grid>
            <Grid item xs={4}>
                <SummaryCard title='title' number={55} icon={<HomeMax />} />
                <SummaryCardSkeleton title='title' number={55} icon={<HomeMax />} />
            </Grid>
            <Grid item xs={4}>
                <OrdersCard
                    data={{
                        id: 1,
                        status: 'completed',
                        number: 1,
                        price: 12,
                        date: '12/12/2022 5:5PM',
                        country: 'KW',
                        name: 'Aous Mohammad'
                    }}
                />
                <OrdersCardSkeleton
                    data={{
                        id: 1,
                        status: 'completed',
                        number: 1,
                        price: 12,
                        date: '12/12/2022 5:5PM',
                        country: 'KW',
                        name: 'Aous Mohammad'
                    }}
                />
            </Grid>
            <Grid item xs={4}>
                <ProductCard
                    data={{
                        index: 1,
                        number: 1,
                        name: 'Aous Mohammad',
                        imgUrl: '',
                        type: 'mobile'
                    }}
                />
            </Grid>
            <Grid item xs={12}>
                <BigProductCard
                    data={{
                        index: 1,
                        number: 1,
                        name: 'Aous Mohammad',
                        imgUrl: '',
                        type: 'mobile'
                    }}
                />
            </Grid>
            <Grid item xs={4}>
                <TaskCard
                />
            </Grid>
            <Grid item xs={4}>
                <TaskCard
                />
            </Grid>
            <Grid item xs={4}>
                <TaskCard
                />
            </Grid>
            <Grid item xs={12}>
                <BasicTabs />
            </Grid>
            <Grid item xs={12} sx={{ background: 'grey' }}>
                <TemporaryDrawer />
            </Grid>
            <Grid item xs={12} sx={{ background: 'grey' }}>
                <HorizontalNonLinearStepper />
            </Grid>
        </Grid>
    )
}