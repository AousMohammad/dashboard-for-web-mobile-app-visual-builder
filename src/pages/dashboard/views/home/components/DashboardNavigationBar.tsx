import { ChevronRight } from '@mui/icons-material'
import { Typography } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'

export const DashboardNavigationBar: React.FC<TDashboardNavigationBar> = ({ navigationBar }) => {
    if (navigationBar.length < 1)  {
        console.error("The Array should be contains more than one element");
        return (
            <React.Fragment></React.Fragment>
        )
    }
    const lastIndex = navigationBar.length - 1;
    const navigationBarArray = navigationBar.reduce((acc, data, index) => {
        acc.push((
            <Typography
                sx={{ cursor: "pointer"}}
                variant={index === lastIndex ? 'darkB' : "grey"}
                onClick={data.onClick}>
                    {data.title}
            </Typography>)
        )
        if (lastIndex !== index) acc.push(<ChevronRight />)
        return acc
    }, [] as Array<React.ReactElement>)
    
    return (
        <Box display="flex" justifyContent="flex-start" alignItems="center" columnGap={1}>
            {navigationBarArray}
        </Box>
    )
}

type TDashboardNavigationBar = {
    navigationBar: Array<{
        title: string,
        onClick: () => void
    }>
}
