import React from 'react';
import { useTranslation } from 'react-i18next';
import { Analytics, Categories, Customers, DeliveryPickup, Design, Domain, Integrations, Orders, Payment, Products, Settings, Vouchers } from 'icons'
import { useTheme } from '@mui/material';


export const useHomeToolsData = () => {
    const { t } = useTranslation();
    const theme = useTheme()
    return [{
        variant: "iron",
        title: t("orders"),
        Icon: () => <Orders />,
        svgColor: theme.palette.iron,
        bgColor: theme.palette.ironOP
    }, {
        variant: "orange",
        title: t("customers"),
        Icon: () => <Customers />,
        svgColor: theme.palette.warning.main,
        bgColor: theme.palette.warning.opacity,
    }, {
        variant: "brown",
        title: t("categories"),
        Icon: () => <Categories  />,
        svgColor: theme.palette.brown,
        bgColor: theme.palette.brownOP
    }, {
        variant: "pink",
        title: t("products"),
        Icon: () => <Products />,
        svgColor: theme.palette.pink,
        bgColor: theme.palette.pinkOP
    }, {
        variant: "blue",
        title: t("analytics"),
        Icon: () => <Analytics />,
        svgColor: theme.palette.blue,
        bgColor: theme.palette.blueOP
    }, {
        variant: "lightBlue",
        title: t("delivery_pickup"),
        Icon: () => <DeliveryPickup  />,
        svgColor: theme.palette.lightBlue,
        bgColor: theme.palette.lightBlueOP
    }, {
        variant: "purple",
        title: t("vouchers"),
        Icon: () => <Vouchers />,
        svgColor: theme.palette.purple,
        bgColor: theme.palette.purpleOP
    }, {
        variant: "green",
        title: t("payment_methods"),
        Icon: () => <Payment />,
        svgColor: theme.palette.green,
        bgColor: theme.palette.greenOP
    }, {
        variant: "grey",
        title: t("account_settings"),
        Icon: () => <Settings />,
        svgColor: theme.palette.grey["A700"],
        bgColor: theme.palette.grey["300"]
    }, {
        variant: "springGreen",
        title: t("integrations"),
        Icon: () => <Integrations />,
        svgColor: theme.palette.springGreen,
        bgColor: theme.palette.springGreenOP
    }, {
        variant: "electricBlue",
        title: t("domain_settings"),
        Icon: () => <Domain />,
        svgColor: theme.palette.electricBlue,
        bgColor: theme.palette.electricBlueOP
    }, {
        variant: "goldenrod",
        title: t("website_design"),
        Icon: () => <Design />,
        svgColor: theme.palette.goldenrod,
        bgColor: theme.palette.goldenrodOP
    }]
}