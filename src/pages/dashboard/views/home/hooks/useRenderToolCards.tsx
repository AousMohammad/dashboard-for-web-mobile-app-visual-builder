import React from "react";
import { useHomeToolsData } from "./useHomeToolsData";
import { Card, Grid, GridProps, Typography } from "@mui/material";

export const useRenderToolCards = (gridProps?: GridProps) => {
    const homeToolsData = useHomeToolsData();
    return React.useCallback(() => (
        homeToolsData.map(({ title, Icon, variant, svgColor, bgColor}) => (
            <Grid xs={2.9 } {...(gridProps || {})} item>
                <Card 
                    sx={ (theme) => ({...theme.mixins.flexCenter, ...theme.mixins.toolsCard(bgColor as string, svgColor as string)})}
                >
                    <Icon  />
                    <Typography>{title}</Typography>
                </Card>
            </Grid>))
    ), [homeToolsData, gridProps]);
}