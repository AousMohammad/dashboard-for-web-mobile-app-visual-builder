import React from 'react';
import { Box, Card, Grid, Stack, Typography } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { useAccountSettingData } from '../hooks';

export const AccountSetting = () => {
    const { t } = useTranslation()
    const ACCOUNT_SETTINGS_DATA = useAccountSettingData()
    return (
        <Grid container xs={12} spacing={3}>
            <Grid item xs={12}>
                <Typography style={{ marginInlineStart: '1vw' }} variant="headerDashboard">{t("account_settings")}</Typography>
            </Grid>
            {
                ACCOUNT_SETTINGS_DATA.map(({ title, subTitle, Icon, lightColor, darkColor }) => (
                    <Grid key={title} item xs={4}>
                        <Card variant="accountSetting">
                            <Box display="flex" alignItems="center" columnGap={2} sx={(theme) => ({ "& svg path": { fill: theme.palette.mode === "dark" ? darkColor : lightColor } })} >
                                <Icon />
                                <Stack>
                                    <Typography variant="dark">{title}</Typography>
                                    <Typography variant="grey">{subTitle}</Typography>
                                </Stack>

                            </Box>
                        </Card>
                    </Grid>
                ))
            }
        </Grid>
    )
}