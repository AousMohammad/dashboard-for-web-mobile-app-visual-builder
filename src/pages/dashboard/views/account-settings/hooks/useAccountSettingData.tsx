import React from 'react'
import { AccountSettingsIcon, BillingIcon, GlobalSolidIcon, InvoiceIcon, NotificationSIcon, StaffIcon, TaxIcon, WebInfoIcon } from 'icons'
import { useTranslation } from 'react-i18next'


export const useAccountSettingData = () => {
    const { t } = useTranslation()
    return [{
        title: t("Account_Details"),
        subTitle: t("account_details_subTitle"),
        Icon: () =><AccountSettingsIcon />,
        lightColor: "#8688a3",
        darkColor: ""
    }, {
        title: t("Languages_Countries"),
        subTitle: t("Languages_Countries_subTitle"),
        Icon: () =><GlobalSolidIcon />,
        lightColor: "#9381FF",
        darkColor: ""
    }, {
        title: t("Staff_Management"),
        subTitle: t("Staff_Management_subTitle"),
        Icon: () =><StaffIcon />,
        lightColor: "#02C39A",
        darkColor: ""
    }, {
        title: t("Website_Information"),
        subTitle: t("Website_Information_subTitle"),
        Icon: () =><WebInfoIcon />,
        lightColor: "#02C39A",
        darkColor: ""
    }, {
        title: t("Invoice_Settings"),
        subTitle: t("Invoice_Settings_subTitle"),
        Icon: () =><InvoiceIcon />,
        lightColor: "#8DC73F",
        darkColor: ""
    }, {
        title: t("Tax_Settings"),
        subTitle: t("Tax_Settings_subTitle"),
        Icon: () =><TaxIcon />,
        lightColor: "#efca08",
        darkColor: ""
    }, {
        title: t("Notifications_Settings"),
        subTitle: t("Notifications_Settings_subTitle"),
        Icon: () =><NotificationSIcon />,
        lightColor: "#fb8500",
        darkColor: ""
    }, {
        title: t("Billing_Plans"),
        subTitle: t("Billing_Plans_subtitle"),
        Icon: () => <BillingIcon />,
        lightColor: "#D54CFF",
        darkColor: ""
    }]
}