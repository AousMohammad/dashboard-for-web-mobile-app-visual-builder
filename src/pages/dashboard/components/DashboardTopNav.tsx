import React from 'react'
import { Avatar, Box, Chip, Divider, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, Toolbar, Typography, useMediaQuery, useTheme } from '@mui/material'
import { AppBar } from 'components/layout/Layout'
import useAppSettings from 'hooks/useAppSettings'
import _ from 'lodash'
import i18n from "../../../i18n";
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import ThemeContext from 'contexts/ThemeContext';
import LangContext from 'contexts/LangContext';
import {
    EditProfile,
    MyWallet,
    MySubscription,
    MyTasks,
    AboutOverZaki,
    HelpSupport,
    ContactUs,
    DarkMode,
    Logout,
} from '../../../icons';

const ProfileListData = [{
    label: "Edit Profile",
    IconComponent: EditProfile
}, {
    label: "My Wallet",
    IconComponent: MyWallet
}, {
    label: "My Subscription",
    IconComponent: MySubscription
}, {
    label: "My Tasks",
    IconComponent: MyTasks
}, {
    label: "About Over Zaki",
    IconComponent: AboutOverZaki
}, {
    label: "Help & Support",
    IconComponent: HelpSupport
}, {
    label: "Contact Us",
    IconComponent: ContactUs
}, {
    label: "Dark Mode",
    IconComponent: DarkMode
}, {
    label: "Logout",
    IconComponent: Logout
}]


const fakeData: TFakeData[] = [{
    websiteName: "Shoppi.com",
    websiteDomain: "http://www.shoppi.com",
    websiteLogo: `${process.env.PUBLIC_URL}/assets/login.png`,
    websiteCategorie: "Website",
    websiteState: "Active"
}, {
    websiteName: "Green.com",
    websiteDomain: "http://www.Green.com",
    websiteLogo: `${process.env.PUBLIC_URL}/assets/login.png`,
    websiteCategorie: "Website",
    websiteState: "Active"
}, {
    websiteName: "Food Tank",
    websiteDomain: "http://www.food-tank.com",
    websiteLogo: `${process.env.PUBLIC_URL}/assets/login.png`,
    websiteCategorie: "Mobile App",
    websiteState: "Expired"
}]

type TFakeData = {
    websiteName: string,
    websiteDomain: string,
    websiteLogo: string,
    websiteCategorie: string,
    websiteState: string
}

type TApplicationListProps = {
    selectedApp: TFakeData,
    apps: TFakeData[],
    onAppClick: (app: TFakeData) => void;
}
const ApplicationList: React.FC<TApplicationListProps> = ({ selectedApp, apps, onAppClick }) => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    const theme = useTheme();
    return (
        <React.Fragment>
            <Box sx={{ cursor: "pointer", height: "40px" }} component="div" onClick={handleClick} display="flex" justifyContent="start" alignItems="center" columnGap={1}>
                <img style={{ filter: theme.palette.mode === 'dark' ? 'invert(1)' : '' }} width="40px" height="40px" src={selectedApp.websiteLogo} alt='' />
                <Box display="flex" flexDirection="column">
                    <Box display="flex" justifyContent="space-between">
                        <Typography>{selectedApp.websiteName}</Typography>
                        <img style={{ filter: theme.palette.mode === 'dark' ? 'invert(1)' : '' }} src={`${process.env.PUBLIC_URL}/assets/icons/chevron-down-down.svg`} alt='' />
                    </Box>
                    <Typography
                        sx={{
                            color: theme.palette.grey['A700'],
                        }}
                    >{selectedApp.websiteDomain}</Typography>
                </Box>
            </Box>
            <Menu
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                PaperProps={{
                    sx: {
                        top: "69px !important",
                        width: "335px",
                        height: "300px",
                        boxShadow: theme.shadows?.[4],
                        borderRadius: "16px",
                        opacity: 1,
                    },
                }}
            >
                {apps.map((app) => (
                    <MenuItem key={app.websiteName} onClick={() => { onAppClick(app); handleClose() }}>
                        <img style={{ boxShadow: theme.shadows?.[5], marginInlineEnd: "10px", filter: theme.palette.mode === 'dark' ? 'invert(1)' : '' }} width="50px" height="50px" src={app.websiteLogo} alt='' />
                        <Box display="flex" flexDirection="column">
                            <Typography sx={{
                                color: theme.palette.secondary.main,
                            }}
                            >{app.websiteName}</Typography>
                            <Typography
                                sx={{
                                    color: theme.palette.grey['A700'],
                                }}
                            >{app.websiteCategorie}</Typography>
                        </Box>
                        <Chip
                            label={app.websiteState}
                            sx={{
                                borderRadius: "16px",
                                ml: "auto",
                                "& .MuiChip-label": {
                                    color: app.websiteState === "Active" ? theme.palette.success.main : theme.palette.error.main,
                                }
                            }}
                        />
                    </MenuItem>
                ))}
            </Menu>
        </React.Fragment>
    )
}


interface LanguageMapping {
    [key: string]: string;
}

export const SelectLanguage: React.FC = () => {
    const { setAppLang, lang: appLang } = useAppSettings();
    const { setLang } = React.useContext(LangContext);
    const { dark, toggle } = React.useContext(ThemeContext);

    const languages = ["English", "French", "العربية"];
    const languageCodes: LanguageMapping = { English: "en", French: "fr", "العربية": "ar" };

    const [selectedLanguage, setSelectedLanguage] = React.useState(languages[appLang === "en" ? 0 : appLang === "fr" ? 1 : 2]);

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const theme = useTheme();
    const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLanguageChange = (language: string) => {
        let langCode = languageCodes[language];

        setLang(langCode);
        setAppLang(langCode);
        setSelectedLanguage(language);
        i18n.changeLanguage(langCode);

        handleClose();
    }

    return (
        <React.Fragment>
            <Box>
                <IconButton sx={{ ml: 1 }} onClick={toggle}>
                    {!dark ? <Brightness7Icon /> : <Brightness4Icon />}
                </IconButton>
            </Box>
            <Box
                component="div"
                onClick={handleClick}
                sx={{
                    width: "120px",
                    height: "40px",
                    background: `${theme.palette.common.white} 0% 0% no-repeat padding-box`,
                    borderRadius: "30px",
                    opacity: 1,
                    display: "flex",
                    justifyContent: "space-evenly",
                    alignItems: "center",
                    cursor: "pointer"
                }}
            >
                <img width="20px" height="20px" alt='' src={`${process.env.PUBLIC_URL}/assets/icons/language_icon.svg`} />
                <Typography sx={{ color: theme.palette.secondary.main }}>{selectedLanguage}</Typography>
                <img width="12px" height="8px" src={`${process.env.PUBLIC_URL}/assets/icons/chevron-down-lang.svg`} alt='' />
            </Box>
            <Menu
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                PaperProps={{
                    sx: {
                        top: "69px !important",
                        left: "902px",
                        width: "160px",
                        height: "140px",
                        boxShadow: theme.shadows?.[4],
                        borderRadius: "16px",
                        opacity: 1,
                    },
                }}
            >
                {languages.map((language) => (
                    <MenuItem key={language} onClick={() => { handleLanguageChange(language); }}>
                        <ListItemText>{language}</ListItemText>
                    </MenuItem>
                ))}
            </Menu>
        </React.Fragment>
    );
};

export const ProfileList: React.FC = () => {
    const dividerNumber = [3, 6, 7];
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    const theme = useTheme();
    return (
        <React.Fragment>
            <Box
                onClick={handleClick}
                component="div"
                display="flex"
                justifyContent="space-evenly"
                alignItems="center"
                columnGap={1}
                sx={{
                    cursor: "pointer"
                }}
            >
                <Avatar sx={{ width: "40px", height: "40px" }} alt='' src={`${process.env.PUBLIC_URL}/assets/icons/User.svg`} />
                <Typography sx={{
                    color: theme.palette.common.black
                }}>
                    Maher Alkhawndi
                </Typography>
                <img src={`${process.env.PUBLIC_URL}/assets/icons/chevron-down-down.svg`} alt='' />
            </Box>
            <Menu
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                PaperProps={{
                    sx: {
                        top: "69px !important",
                        left: "1086px",
                        width: "260px",
                        height: "446px",
                        boxShadow: theme.shadows?.[5],
                        borderRadius: "16px",
                        opacity: 1,
                    },
                }}
            >
                {
                    ProfileListData.map((data, ind) => {
                        return (
                            <li key={_.uniqueId()}>
                                <MenuItem sx={{ mb: 1 }} key={data.label} onClick={handleClose}>
                                    <ListItemIcon>
                                        <data.IconComponent width="20px" height="20px" />
                                    </ListItemIcon>
                                    <ListItemText>
                                        {data.label}
                                    </ListItemText>
                                </MenuItem>
                                {dividerNumber.includes(ind) ? <Divider sx={{ mb: 1 }} orientation='horizontal' variant="middle" /> : null}
                            </li>
                        )
                    })
                }
            </Menu>
        </React.Fragment>
    )
}
export const DashboardTopNav: React.FC = () => {
    const [selectedApp, setSelectedApp] = React.useState<TFakeData>(() => fakeData[0])
    const theme = useTheme();
    return (
        <AppBar enableColorOnDark>
            <Toolbar
                sx={{
                    pr: '24px',
                    display: "flex",
                    justifyContent: "space-between",
                    backgroundColor: theme.palette.common.white,
                }}
            >
                <Box display='flex' width='11.5vw' sx={{ borderRight: `1px solid ${theme.palette.grey['400']}` }}>
                    <img width="150px" height="30px" alt='Logo' src={`${process.env.PUBLIC_URL}/assets/icons/OZ-Logo.svg`} />
                </Box>
                <Box flexGrow={1} display="flex" justifyContent="space-between" ml={3}>
                    <ApplicationList selectedApp={selectedApp} apps={fakeData} onAppClick={(app) => setSelectedApp(app)} />
                    <Box mr={1} display="flex" alignItems="center" columnGap={4}>
                        <SelectLanguage />
                        <img style={{ cursor: "pointer" }} alt='' src={`${process.env.PUBLIC_URL}/assets/icons/notification_icon.svg`} />
                        <ProfileList />
                    </Box>
                </Box>
            </Toolbar>
        </AppBar>
    )
}