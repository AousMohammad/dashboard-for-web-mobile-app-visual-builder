import { Box, Typography } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'

const Widgets: React.FC = () => {
    const { t } = useTranslation();
    return (
        <Box display="flex" flexDirection="column">
            <img src={process.env.PUBLIC_URL + "/assets/websites.svg"} alt="website icon" />
            <Typography mt={2}>{t('websites')}</Typography>
        </Box>
    )
}

export default Widgets