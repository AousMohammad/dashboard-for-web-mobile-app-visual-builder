import React from 'react'
import { Outlet } from 'react-router-dom';
import { useMediaQuery, Box, Grid, useTheme } from '@mui/material';
import { EcoAppBar } from 'components/EcoAppBar';


export const SelectionPage: React.FC = () => {
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'))
    return (
        <Box width="100%" height="100%">
            <EcoAppBar />
            <Grid container paddingX={matches ? 15 : 2}>
                <Outlet />
            </Grid>
        </Box>
    )
}
