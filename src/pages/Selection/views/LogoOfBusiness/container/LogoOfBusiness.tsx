import React from 'react'
import { Grid, Typography, Stack, useTheme, useMediaQuery, Chip } from '@mui/material'
import { AuthNewRightSide, NextBtn } from 'pages/authinication/components'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
// import { useForm } from 'react-hook-form'
import InfoIcon from '@mui/icons-material/Info';

type Props = {}

export const LogoOfBusiness: React.FC = (props: Props) => {
    const { t } = useTranslation();
    // const { register, watch } = useForm<any>()
    // const [logo] = watch(["logo"]);
    const nextBtnImgSrc = `${process.env.PUBLIC_URL}/assets/icons/chevron-right-dark.svg`;
    const navigate = useNavigate();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'))
    return (
        <>
            <Grid item xs={12} md={12} height="50vh" display="flex" alignItems="center" justifyContent="center" overflow="hidden">
                <AuthNewRightSide url="addlogo" />
            </Grid>
            <Grid item xs={12} px={2} md={12} mt={6}>
                <Typography textAlign="center" variant={matches ? 'h3' : 'h4'} fontWeight="800">
                    {t('what_logo')} <span style={{ color: theme.palette.grey.A700 }}>{t('business')}?</span>
                </Typography>
            </Grid>
            <Grid item xs={12} md={12} display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                <Stack rowGap={1} width="100%" height="45vh" justifyContent="center" alignItems="center">
                    <Chip icon={<InfoIcon />} label={t('skip_slogan')} />
                    <div style={{ flexDirection: 'column', width: '75%', height: '75%', border: '4px dashed #EBEBF0', borderRadius: '15px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <img src={process.env.PUBLIC_URL + '/assets/icons/upload_icon.svg'} alt="Upload Icon" />
                        <span style={{ marginTop: '5%' }}>{t('upload_logo')}</span>
                    </div>
                </Stack>
                <NextBtn onClick={() => navigate("/selection/theme")} isActive={true} >
                    <img
                        style={{ marginInlineEnd: '3%' }}
                        src={nextBtnImgSrc} alt="Google Logo"
                    />
                </NextBtn>
            </Grid>
        </>
    )
}