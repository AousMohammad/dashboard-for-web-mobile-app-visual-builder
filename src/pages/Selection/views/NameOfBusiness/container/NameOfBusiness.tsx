import React from 'react'
import { FormControl, Grid, InputLabel, Stack, TextField, Typography, useMediaQuery, useTheme } from '@mui/material'
import { useTranslation } from 'react-i18next'
import { useForm } from 'react-hook-form'
import { AuthNewRightSide, NextBtn } from 'pages/authinication/components'
import { useNavigate } from 'react-router-dom'


export const NameOfBusiness: React.FC = () => {
    const { t } = useTranslation();
    const { register, formState: { errors }, watch } = useForm<any>()
    const [englishName, arabicName] = watch(["englishName", "arabicName"]);
    const nextBtnImgSrc = `${process.env.PUBLIC_URL}/assets/icons/chevron-right-${englishName ? "dark" : "gray"}.svg`;
    const navigate = useNavigate();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'))

    return (
        <>
            <Grid item xs={12} md={12} height="50vh" display="flex" alignItems="center" justifyContent="center" overflow="hidden">
                <AuthNewRightSide url="businessname" />
            </Grid>
            <Grid item xs={12} px={2} md={12} mt={6}>
                <Typography textAlign="center" variant={matches ? 'h3' : 'h4'} fontWeight="800">
                    {t('what_name')} <span style={{ color: theme.palette.grey.A700 }}>{t('business')}?</span>
                </Typography>
            </Grid>
            <Grid item xs={12} md={12} display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                <Stack rowGap={1} width="100%" height="40vh" justifyContent="center">
                    <FormControl>
                        <InputLabel>
                            {t('english_name')}
                        </InputLabel>
                        <TextField
                            fullWidth
                            value={englishName}
                            sx={{ mb: 3, borderRadius: "16px" }}
                            {...register('englishName', { required: t('required') })}
                            error={!!errors.englishName}
                            placeholder={t("english_name")}
                        />
                    </FormControl>
                    <FormControl>
                        <InputLabel>
                            {t('arabic_name')}
                        </InputLabel>
                        <TextField
                            fullWidth
                            value={arabicName}
                            sx={{ mb: 3, borderRadius: "16px" }}
                            {...register('arabicName')}
                            error={!!errors.arabicName}
                            placeholder={t("arabic_name")}
                        />
                    </FormControl>
                </Stack>
                <NextBtn onClick={() => { if (englishName && Boolean(englishName)) navigate("/selection/logo") }} isActive={Boolean(englishName)} >
                    <img
                        style={{ marginInlineEnd: '3%' }}
                        src={nextBtnImgSrc} alt="Google Logo"
                    />
                </NextBtn>
            </Grid>
        </>
    )
}