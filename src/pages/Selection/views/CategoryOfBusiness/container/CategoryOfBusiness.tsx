import React from 'react'
import { Grid, Typography, useTheme, useMediaQuery } from '@mui/material'
import { t } from 'i18next'
import { AuthNewRightSide, LanguageBtn, NextBtn } from 'pages/authinication/components'
import { useNavigate } from 'react-router-dom'
import withSkeleton from 'components/HOC/withSkeleton'
import Widgets from 'pages/Selection/components/Widgets'

const SkeletonWrappedComponent = withSkeleton(Widgets);

export const CategoryOfBusiness: React.FC = () => {
    const navigate = useNavigate();
    const [category, setCategory] = React.useState<string | undefined>();
    const nextBtnImgSrc = `${process.env.PUBLIC_URL}/assets/icons/chevron-right-${category ? "dark" : "gray"}.svg`;
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'))

    return (
        <>
            <Grid item xs={12} md={12} height="50vh" display="flex" alignItems="center" justifyContent="center" overflow="hidden">
                <AuthNewRightSide url="whattype" />
            </Grid>
            <Grid item xs={12} md={12} px={2} mt={6}>
                <Typography textAlign="center" variant={matches ? 'h3' : 'h4'} fontWeight="800">
                    {t('what_category')} <span style={{ color: theme.palette.grey.A700 }}>{t('business')}?</span>
                </Typography>
            </Grid>
            <Grid item xs={12} md={12}>
                <Typography variant='h6' mt={2}>{t('stores')}</Typography>
            </Grid>
            <Grid container flexWrap={!matches ? 'nowrap' : 'wrap'} overflow={!matches ? 'scroll' : 'none'}>
                <Grid item marginX={!matches ? 3 : 0} xs={4} md={2} display="flex" my={6} flexDirection="column" justifyContent="center" alignItems="center">
                    <LanguageBtn isActive={category === "website"} onClick={() => setCategory('website')} name="website">
                        <SkeletonWrappedComponent />
                    </LanguageBtn>
                </Grid>
                <Grid item marginX={!matches ? 3 : 0} xs={4} md={2} display="flex" my={6} flexDirection="column" justifyContent="center" alignItems="center">
                    <LanguageBtn isActive={category === "website"} onClick={() => setCategory('website')} name="website">
                        <SkeletonWrappedComponent />
                    </LanguageBtn>
                </Grid>
                <Grid item marginX={!matches ? 3 : 0} xs={4} md={2} display="flex" my={6} flexDirection="column" justifyContent="center" alignItems="center">
                    <LanguageBtn isActive={category === "website"} onClick={() => setCategory('website')} name="website">
                        <SkeletonWrappedComponent />
                    </LanguageBtn>
                </Grid>
                <Grid item marginX={!matches ? 3 : 0} xs={4} md={2} display="flex" my={6} flexDirection="column" justifyContent="center" alignItems="center">
                    <LanguageBtn isActive={category === "website"} onClick={() => setCategory('website')} name="website">
                        <SkeletonWrappedComponent />
                    </LanguageBtn>
                </Grid>
            </Grid>
            <Grid item xs={12} md={12} display="flex" my={6} justifyContent="center" alignItems="center">
                <NextBtn onClick={() => { if (category && Boolean(category)) navigate("/selection/name") }} isActive={Boolean(category)} >
                    <img
                        style={{ marginInlineEnd: '3%' }}
                        src={nextBtnImgSrc} alt="Google Logo"
                    />
                </NextBtn>
            </Grid>
        </>
    )
}