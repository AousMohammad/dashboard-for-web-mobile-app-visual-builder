import React from 'react'
import { useTranslation } from 'react-i18next'

type Props = {}

const Dashboard = (props: Props) => {
    const { t } = useTranslation();
    return (
        <div>s{t('welcome')}</div>
    )
}

export default Dashboard