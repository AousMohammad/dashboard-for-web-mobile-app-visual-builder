import React from 'react'
import { Box, Button } from '@mui/material'
import { DropdownList } from 'components'
import { ToggleDevices } from '../components/ToggleDevices'
import { Switcher } from 'components/Switcher'
import { Add } from '@mui/icons-material'
import { MockUp } from '../components/MockUp'
import { Outlet } from 'react-router-dom'
import { useRecoilState } from 'recoil'
import { activeTab } from 'state/atom'

export const Design: React.FC = () => {
    const [value, setValue] = useRecoilState(activeTab);
    return (
        <div>
            <Outlet />
            <Box sx={{ width: '50vw' }}>
                <Box width='100%' display='flex' justifyContent='space-between'>
                    <DropdownList />
                    <Switcher />
                    <Button color='secondary'>
                        <Add />
                        Add Secion
                    </Button>
                </Box>
                <Box width='100%' mt={5} display='flex' alignItems='center' justifyContent='center' flexDirection='column'>
                    <ToggleDevices />
                    <MockUp />
                    <Button variant='contained' sx={{ width: '250px', borderRadius: '20px' }} onClick={() => setValue(value + 1)}>
                        Next
                    </Button>
                </Box>
            </Box>
        </div>
    )
}