import { Box, Button, Slider } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useRecoilState } from 'recoil';
import { mockUp } from 'state/atom';

type Props = {}

function valuetext(value: number) {
    return `${value}px`;
}

export const ButtonDesign: React.FC = (props: Props) => {
    const { t } = useTranslation();
    const marks = [
        {
            value: 0,
            label: t('sharp'),
        },
        {
            value: 100,
            label: t('rounded'),
        },
    ];
    const [value, setValue] = useRecoilState(mockUp);
    return (
        <Box sx={{ width: '100%', display: 'flex', mt: 5, justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
            <Slider onChange={(e, newVal) => setValue({
                ...value,
                style: {
                    ...value.style,
                    button: {
                        borderRadius: (newVal as number) / 4,
                    }
                }
            })} style={{ width: '80%' }} defaultValue={1} getAriaValueText={valuetext} valueLabelDisplay="auto" marks={marks} />
            <Button
                color='secondary'
                sx={{ mt: 5, borderRadius: `${value.style.button.borderRadius}px`, width: '100%', backgroundColor: '#8688A3', color: 'white', height: '50px' }}
            >
                Button Sample
            </Button>
        </Box>
    )
}