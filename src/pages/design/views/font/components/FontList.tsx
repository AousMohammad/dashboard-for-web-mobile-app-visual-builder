import React from 'react'
import GoogleFontsPicker from '../../../components/GoogleFontsPicker'
import { FormControl, FormLabel, RadioGroup } from '@mui/material'
import { useRecoilState } from 'recoil'
import { mockUp } from 'state/atom'

type Props = {}

export const FontList: React.FC = (props: Props) => {
    const [selectedFont, setSelectedFont] = React.useState<string>('');
    const [style, setStyle] = useRecoilState(mockUp);
    const handleFontChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSelectedFont(event.target.value);
        setStyle({
            ...style,
            style: {
                ...style.style,
                font: event.target.value,
            }
        })

    };

    return (
        <FormControl>
            <FormLabel id="demo-radio-buttons-group-label">Fonts</FormLabel>
            <RadioGroup
                aria-labelledby="demo-radio-buttons-group-label"
                defaultValue="female"
                name="radio-buttons-group"
            >
                <GoogleFontsPicker selectedFont={selectedFont} handleFontChange={handleFontChange} />
            </RadioGroup>
        </FormControl>
    )
}