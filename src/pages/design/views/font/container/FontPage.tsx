import React from 'react'
import { ButtonDesign, FontList } from '../components'
import { useTranslation } from 'react-i18next'
import { VerticalTabs } from 'pages/design/components'
import { ButtonIcon, FontIcon } from 'icons'
import UploadTemplate from 'components/UploadTemplate'
//theme
import "primereact/resources/themes/lara-light-indigo/theme.css";     
    
//core
import "primereact/resources/primereact.min.css"; 

export const FontPage: React.FC = () => {
    const { t } = useTranslation();
    return (
        <div>
            <VerticalTabs tabs={[{
                tab: <FontList />,
                title: t('font'),
                icon: <FontIcon />
            }, {
                tab: <ButtonDesign />,
                title: t('buttons'),
                icon: <ButtonIcon />
            }, {
                tab: <UploadTemplate />,
                title: 'logo',
                icon: <ButtonIcon />
            }]} />
        </div>
    )
}