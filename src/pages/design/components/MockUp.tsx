import React from 'react'
import { Box, Button, Typography } from '@mui/material'
import { useRecoilState, useRecoilValue } from 'recoil';
import { deviceState, mockUp } from 'state/atom';

export const MockUp: React.FC = () => {
    const state = useRecoilState(mockUp);
    const device = useRecoilValue(deviceState);
    console.log(device);
    return (
        <Box
            sx={{
                mt: 5,
                width: device === 'phone' ? '300px' : device === 'tv' ? '500px' : '700px',
                height: '500px',
                borderRadius: '20px',
                border: '8px solid black',
                overflow: 'hidden',
                backgroundColor: 'white',
            }}
        >
            <Box
                sx={{
                    width: '100%',
                    height: '100%',
                    overflow: 'auto',
                    padding: '30px',
                }}
            >
                <Typography fontFamily={state[0].style.font}>
                    SAMPLE TEXT
                    <br /><br />
                </Typography>
                <Button
                    color='secondary'
                    sx={{ mt: 5, fontFamily: state[0].style.font, borderRadius: `${state[0].style.button.borderRadius}px`, width: '-webkit-fill-available', backgroundColor: '#8688A3', color: 'white', height: '50px' }}
                >
                    Button Sample
                </Button>
            </Box>
        </Box>
    )
}