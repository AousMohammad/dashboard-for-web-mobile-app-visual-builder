import React, { useEffect, useState } from 'react';
import { FormControlLabel, Radio } from '@mui/material';
import InfiniteScroll from 'react-infinite-scroll-component';

type Props = {
    selectedFont: string;
    handleFontChange: (event: any) => void
}

const ITEMS_PER_PAGE = 50; // Define how many items to load per page

export const GoogleFontsPicker: React.FC<Props> = ({ selectedFont, handleFontChange }) => {
    const [fonts, setFonts] = useState<string[]>([]);
    const [displayCount, setDisplayCount] = useState(ITEMS_PER_PAGE);
    const [hasMore, setHasMore] = useState(true);

    const fetchFonts = async () => {
        try {
            const response = await fetch(`https://www.googleapis.com/webfonts/v1/webfonts?key=${process.env.REACT_APP_GOOGLE_API_KEY}`);
            const data = await response.json();

            if (data.items.length > 0) {
                const allFonts = data.items.map((item: any) => item.family);
                setFonts(allFonts);
            } else {
                setHasMore(false);
            }
        } catch (error) {
            console.error(error);
        }
    };

    const loadMoreFonts = () => {
        if (displayCount >= fonts.length) {
            setHasMore(false);
        } else {
            setDisplayCount(prevCount => prevCount + ITEMS_PER_PAGE);
        }
    };

    useEffect(() => {
        fetchFonts();
    }, []);

    useEffect(() => {
        // Load the next portion into the document head
        fonts.slice(0, displayCount).forEach((font: string) => {
            const link = document.createElement('link');
            link.href = `https://fonts.googleapis.com/css2?family=${encodeURIComponent(font)}&display=swap`;
            link.rel = 'stylesheet';
            document.head.appendChild(link);
        });
    }, [displayCount]);

    useEffect(() => {
        if (selectedFont) {
            const link = document.createElement('link');
            link.href = `https://fonts.googleapis.com/css2?family=${encodeURIComponent(selectedFont)}&display=swap`;
            link.rel = 'stylesheet';
            document.head.appendChild(link);
        }
    }, [selectedFont]);

    return (
        <div id="scrollableDiv" style={{ height: '83vh', overflow: 'auto' }}>
            <InfiniteScroll
                dataLength={displayCount}
                next={loadMoreFonts}
                hasMore={hasMore}
                scrollableTarget="scrollableDiv"
                loader={<h4>Loading...</h4>}
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                }}
            >
                {fonts.slice(0, displayCount).map(font => (
                    <FormControlLabel
                        key={font}
                        value={font}
                        control={<Radio />}
                        label={<span style={{ fontFamily: font }}>{font}</span>}
                        onChange={handleFontChange}
                    />
                ))}
            </InfiniteScroll>
        </div>
    );
};

export default GoogleFontsPicker;
