import * as React from 'react';
import { useRecoilState } from 'recoil';
import { activeTab } from 'state/atom';
import { Card, Box, Tab, Tabs } from '@mui/material';
import _ from 'lodash';
import { Tab as TTab } from 'types'

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            {...other}
            style={{ width: '100%', height: '100%' }}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    {children}
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}


type Props = {
    tabs: TTab[];
};

export const VerticalTabs: React.FC<Props> = ({ tabs }) => {
    const [value, setValue] = useRecoilState(activeTab);

    return (
        <Box
            sx={{ flexGrow: 1, bgcolor: 'background.paper', display: 'flex', height: '93vh', position: 'absolute', right: 0, top: '7vh', width: '30vw', justifyContent: 'end' }}
        >
            {
                tabs.map((tab, index) => (
                    <TabPanel key={_.uniqueId()} value={value} index={index}>
                        {tab.tab}
                    </TabPanel>
                ))
            }
            <Tabs
                orientation="vertical"
                variant="scrollable"
                value={value}
                onChange={(event: React.SyntheticEvent, newValue: number) => setValue(newValue)}
                aria-label="Vertical tabs example"
                sx={{ borderInlineStart: '1px solid grey', borderColor: 'divider', width: '7vw' }}
            >
                {
                    tabs.map((tab, index) => (
                        <Tab key={_.uniqueId()} label={
                            <>
                                <Card variant={value === index ? 'smallPrimary' : 'smallLightGrey'}>
                                    <span className={value === index ? 'activeIcon' : ''}>
                                        {tab.icon}
                                    </span>
                                </Card>
                                {tab.title}
                            </>
                        } {...a11yProps(index)} />
                    ))
                }
            </Tabs>
        </Box>
    );
}