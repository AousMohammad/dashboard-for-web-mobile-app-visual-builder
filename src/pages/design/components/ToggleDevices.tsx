import { Stack, ToggleButton, ToggleButtonGroup } from '@mui/material'
import React from 'react'
import LaptopIcon from '@mui/icons-material/Laptop';
import TvIcon from '@mui/icons-material/Tv';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import { useRecoilState } from 'recoil';
import { deviceState } from 'state/atom';

type Props = {}

export const ToggleDevices = (props: Props) => {
    const [devices, setDevices] = useRecoilState(deviceState);
    const handleDevices = (
        event: React.MouseEvent<HTMLElement>,
        newDevices: string,
    ) => {
        if (newDevices) {
            setDevices(newDevices);
        }
    };

    return (
        <Stack direction="row" spacing={4}>
            <ToggleButtonGroup
                value={devices}
                exclusive
                onChange={handleDevices}
                aria-label="device"
            >
                <ToggleButton value="laptop" aria-label="laptop">
                    <LaptopIcon />
                </ToggleButton>
                <ToggleButton value="tv" aria-label="tv">
                    <TvIcon />
                </ToggleButton>
                <ToggleButton value="phone" aria-label="phone">
                    <PhoneAndroidIcon />
                </ToggleButton>
            </ToggleButtonGroup>
        </Stack>
    )
}