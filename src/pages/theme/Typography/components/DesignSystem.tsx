import React from 'react';
import { Typography } from '@mui/material';

interface DesignSystemProps {
    typography: any;
}

export const DesignSystem: React.FC<DesignSystemProps> = ({ typography }) => {
    const fontFamily = typography.fontFamily;
    const typographyStyles = Object.entries(typography).filter(([key]) => key !== 'fontFamily');

    return (
        <div>
            <Typography variant="h6">Font Family: {fontFamily}</Typography>
            {typographyStyles.map(([styleName, style]) => {
                if (typeof style === 'object') {
                    return (
                        <div key={styleName} style={{ marginTop: '5%', marginBottom: '5%' }}>
                            <Typography variant={(styleName as any)}>{styleName} SAMPLE TEXT</Typography>
                            <Typography variant="subtitle1">Font Weight: {(style as any)?.fontWeight!}</Typography>
                            <Typography variant="subtitle1">Color: {(style as any)?.color!}</Typography>
                            <Typography variant="subtitle1">Font Size: {(style as any)?.fontSize!}</Typography>
                        </div>
                    )
                }
                return null
            })}
        </div>
    );
};
