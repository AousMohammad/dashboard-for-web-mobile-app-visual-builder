import { Card, Grid, useTheme } from '@mui/material';
import React from 'react'
import { DesignSystem } from './components/DesignSystem';

type Props = {}

export const TypographyPage = (props: Props) => {
    const theme = useTheme();
    if (!theme.typography.fontFamily) {
        throw new Error('fontFamily is not defined in the theme');
    }
    return (
        <Grid container spacing={5}>
            <Grid item xs={12}>
                <Card variant='white'>
                    <DesignSystem typography={theme.typography} />;
                </Card>
            </Grid>
        </Grid>
    )
}