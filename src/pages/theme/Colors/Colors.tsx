import React from 'react'
import { Card, Grid } from '@mui/material'
import { paletteDark, paletteLight } from 'theme/theme';
import _ from 'lodash';
import { ColorPalette } from './components/ColorPalette';

type Props = {}

export const Colors = (props: Props) => {
    const lightColors = paletteLight;
    const darkColors = paletteDark;
    return (
        <Grid container spacing={5}>
            <Grid item xs={12}>
                Light Mode Colors
                <Card variant='white' style={{ padding: '5%' }}>
                    <Grid container spacing={5}>
                        <ColorPalette palette={lightColors} />
                    </Grid>
                </Card>
            </Grid>
            <Grid item xs={12}>
                Dark Mode Colors
                <Card variant='white' style={{ padding: '5%' }}>
                    <Grid container spacing={5}>
                        <ColorPalette palette={darkColors} />
                    </Grid>
                </Card>
            </Grid>
        </Grid>
    )
}