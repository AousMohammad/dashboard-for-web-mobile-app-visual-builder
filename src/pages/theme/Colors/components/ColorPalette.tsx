import { Grid, useTheme } from '@mui/material';
import _ from 'lodash';
import React from 'react';

type PaletteLight = {
    primary: { main: string, opacity: string },
    secondary: { main: string },
    info: { main: string },
    success: { main: string },
    warning: { main: string, opacity: string },
    error: { main: string, opacity: string },
    grey: { A700: string, A100: string },
    springGreen: string,
    springGreenOP: string,
    electricBlue: string,
    electricBlueOP: string,
    goldenrod: string,
    goldenrodOP: string,
    brown: string,
    brownOP: string,
    pink: string,
    pinkOP: string,
    blue: string,
    blueOP: string,
    lightBlue: string,
    lightBlueOP: string,
    purple: string,
    purpleOP: string,
    green: string,
    greenOP: string,
    iron: string,
    ironOP: string,
    common: { white: string, black: string }
};

type Props = {
    color: string;
    name: string;
};

export const CPaper = (props: Props) => {
    const { color, name } = props;
    const theme = useTheme()
    return (
        <div style={{ marginTop: '2%', marginBottom: '2%', width: '150px', height: '150px', display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center' }}>
            <div style={{ boxShadow: theme.shadows?.["6"] ,backgroundColor: color, height: '100px', width: '100%', borderRadius: '15px', }}></div>
            <span>{color}</span>
            <span>{name}</span>
        </div>
    )
}

export const ColorPalette: React.FC<{ palette: PaletteLight }> = ({ palette }) => {
    const colorArray = Object.entries(palette).flatMap(([name, value]) => {
        if (typeof value === 'string') {
            return { name, color: value };
        } else if (typeof value === 'object') {
            return Object.entries(value).map(([key, color]) => ({ name: `${name}.${key}`, color }));
        }
        return [];
    });

    return (
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {colorArray.map((colorObj, index) => (
                <Grid item key={_.uniqueId()} xs={4} md={2}>
                    <CPaper {...colorObj} />
                </Grid>
            ))}
        </div>
    );
};
