import React from 'react'
import _ from 'lodash';


import CButton from 'components/CButton';

import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useVisibilityPassword } from 'pages/authinication/hooks';
import { InputAdornment, Stack, TextField, Typography, useTheme } from '@mui/material';


export const ResetPasswordForm: React.FC = () => {
    const { t } = useTranslation();
    const { passwordType: newPasswordType, VisibilityIcon: NewPasswordVisibleIcon } = useVisibilityPassword()
    const { passwordType: confirmPasswordType, VisibilityIcon: ConfirmPasswordVisibleIcon } = useVisibilityPassword()

    const { register, formState: { errors }, watch, handleSubmit, setError } = useForm<TResetPasswordForm>()
    const [newPassword, confirmPassword] = watch(["newPassword", "confirmPassword"]);
    const theme = useTheme();
    const disabled = React.useMemo(() => _.isEmpty(newPassword) && _.isEmpty(confirmPassword), [newPassword, confirmPassword])

    const onSubmit = (data: TResetPasswordForm) => {
        if (newPassword !== confirmPassword) {
            setError("confirmPassword", {
                message: t("textField_resetPasswordError")
            }, {
                shouldFocus: true
            });
            return
        }
    }

    return (
        <Stack rowGap={3} mr={2} ml={2} width="100%">
            <Stack rowGap={1} >
                <Typography fontWeight="bolder" component="text" sx={{ color: theme.palette.secondary.main, fontSize: "28px" }}>{t('reset-password')}</Typography>
            </Stack>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Stack rowGap={1}>
                    <TextField
                        label={t('textField_label_newPassword')}
                        type={newPasswordType}
                        fullWidth
                        sx={{ mb: 3, borderRadius: "16px" }}
                        {...register('newPassword', { required: t('required_password') })}
                        error={!!errors.newPassword}
                        helperText={errors.newPassword?.message}
                        placeholder={t("textField_placeHolder_newPassword")}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <NewPasswordVisibleIcon />
                                </InputAdornment>
                            ),
                        }}
                    />
                    <TextField
                        label={t('textField_label_confirmPassword')}
                        type={confirmPasswordType}
                        fullWidth
                        sx={{ mb: 3 }} // textField_resetPasswordError
                        {...register('confirmPassword', { required: t('required_password') })}
                        error={!!errors.confirmPassword}
                        helperText={errors.confirmPassword?.message}
                        placeholder={t("textField_placeHolder_confirmPassword")}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <ConfirmPasswordVisibleIcon />
                                </InputAdornment>
                            ),
                        }}
                    />
                    <CButton fullWidth disabled={disabled} type="submit" borderRadius="lg" color="primary" text={t("reset-password")} />
                </Stack>
            </form>
        </Stack>
    )
}

export type TResetPasswordForm = {
    newPassword: string;
    confirmPassword: string;
};