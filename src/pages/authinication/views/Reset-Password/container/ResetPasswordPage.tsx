import React from 'react'
import { AuthContainer } from 'pages/authinication/container'
import { ResetPasswordForm } from '../components'


export const ResetPasswordPage: React.FC = () => {


    return (
        <AuthContainer AuthNewLeftSideContent={<ResetPasswordForm />} url="reset" />
    )
}