import React from "react";
import { FormValues } from "./SignupForm"
import { useTranslation } from "react-i18next"
import { FormControl, Grid, InputLabel, TextField } from "@mui/material"
import { FormState, UseFormRegister } from "react-hook-form"


export const UserNameFields: React.FC<React.PropsWithChildren<TUserNameFieldsProps>> = ({ register, formState: { errors }, children }) => {
    const { t } = useTranslation()
    return (
        <Grid container spacing={2}>
            <Grid item xs={6}>
                <FormControl>
                    <InputLabel>
                        {t('first_name')}
                    </InputLabel>
                    <TextField
                        type="text"
                        fullWidth
                        sx={{ mb: 3 }}
                        {...register('firstName', { required: t('invalid_firstName') })}
                        error={!!errors.firstName}
                        helperText={errors.firstName?.message}
                        placeholder={t("firstName_placeholder")}
                    />
                </FormControl>
            </Grid>
            <Grid item xs={6}>
                <FormControl variant="standard" fullWidth>
                    <InputLabel shrink htmlFor="bootstrap-input">
                        {t('last_name')}
                    </InputLabel>
                    <TextField
                        type="text"
                        fullWidth
                        sx={{ mb: 3 }}
                        {...register('lastName', { required: t('invalid_lastName') })}
                        error={!!errors.lastName}
                        helperText={errors.lastName?.message}
                        placeholder={t("lastName_placeholder")}
                    />
                </FormControl>
            </Grid>
        </Grid>
    )
}

type TUserNameFieldsProps = {
    register: UseFormRegister<FormValues>,
    formState: FormState<FormValues>
}
