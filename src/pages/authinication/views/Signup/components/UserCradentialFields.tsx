import React from "react";

import { FormValues } from "./SignupForm"
import { useTranslation } from "react-i18next"
import { FormState, UseFormRegister } from "react-hook-form"
import { TextField, InputAdornment, IconButton, FormControl, InputLabel, TextFieldProps } from "@mui/material"
import { useVisibilityPassword } from "pages/authinication/hooks";


export const UserCradentialFields: React.FC<React.PropsWithChildren<TUserCradentialFieldsPerops>> = (
  { children, register, formState: { errors }, emailTextFieldProps, passwordTextFieldProps }) => {
  const { passwordType } = useVisibilityPassword()
  const { t } = useTranslation()
  return (
    <>
      <FormControl>
        <InputLabel>
          {t('email')}
        </InputLabel>
        <TextField
          sx={{ mb: 3 }}
          {...register('email', { required: t('required_email'), pattern: { value: /^\S+@\S+$/i, message: t('invalid_email') } })}
          error={!!errors.email}
          helperText={errors.email?.message}
          placeholder={t("email_placeholder")}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton>
                  &#64;
                </IconButton>
              </InputAdornment>
            ),
          }}
          {...(emailTextFieldProps || {})}
        />
      </FormControl>
      <FormControl>
        <InputLabel>
          {t('password')}
        </InputLabel>
        <TextField
          type={passwordType}
          sx={{ mb: 3 }}
          {...register('password', { required: t('required_password') })}
          error={!!errors.password}
          helperText={errors.password?.message}
          placeholder={t("password_placeholder")}
          {...(passwordTextFieldProps || {})}
        />
      </FormControl>
      {children}
    </>
  )
}

type TUserCradentialFieldsPerops = {
  register: UseFormRegister<FormValues>,
  formState: FormState<FormValues>,
  emailTextFieldProps?: TextFieldProps,
  passwordTextFieldProps?: TextFieldProps
}

