import React from "react";
import CountryComboBox from "./CountryCombo";

import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Typography } from "@mui/material";
import { UserCradentialFields, UserNameFields } from ".";
import { SocialAuthinication } from "pages/authinication/components";
import CButton from "components/CButton";


export const SignupForm = () => {
    const { register, watch, handleSubmit, formState } = useForm<FormValues>();
    const { t } = useTranslation();
    const navigate = useNavigate();

    const onSubmit = (data: FormValues) => {
        console.log(data);
        navigate('/verify', { replace: true })
        // Perform your login action here
    };
    const [firstName, lastName, email, password] = watch(['firstName', 'lastName', 'email', 'password']);
    return (
        <div style={{ display: 'flex', flexDirection: 'column', position: 'relative', alignItems: 'center', width: '100%' }}>
            <SocialAuthinication AuthinicationType="signup" />
            <form onSubmit={handleSubmit(onSubmit)} style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
                <UserNameFields register={register} formState={formState} />
                <CountryComboBox register={register} formState={formState} />
                <UserCradentialFields register={register} formState={formState}>
                    <CButton disabled={!(firstName && lastName && email && password)} type="submit" borderRadius="lg" color="primary" text={t('signup')} />
                </UserCradentialFields>
                <Typography variant="body2" textAlign='center' mt={3}>{t('have_account')} <a href="/login">{t('login')}</a></Typography>
            </form>
        </div>
    );
}

export type FormValues = {
    firstName: string;
    lastName: string;
    country: string;
    email: string;
    password: string;
};