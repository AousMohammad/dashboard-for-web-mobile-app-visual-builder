import React from 'react'

import { SignupForm } from '../components'
import { AuthContainer } from 'pages/authinication/container'

export const SignupPage: React.FC = () => {
    return (
        <AuthContainer AuthNewLeftSideContent={<SignupForm />} url="creataccount" />
    )
}