import React from 'react'
import CButton from 'components/CButton'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { IconButton, InputAdornment, Stack, TextField, Typography, useTheme } from '@mui/material'
import { useNavigate } from 'react-router-dom'

export const ForgetPasswordContent: React.FC = () => {
    const { t } = useTranslation();
    const theme = useTheme();
    const { register, formState: { errors }, watch, handleSubmit } = useForm<TForgetPasswordform>()
    const [email] = watch(["email"]);
    const naviagte = useNavigate();
    const onSubmit = (data: TForgetPasswordform) => {
        naviagte('/verify');
    }
    return (
        <Stack rowGap={3} mr={2} ml={2}>
            <Stack rowGap={1} >
                <Typography fontWeight="bolder" component="text" sx={{ color: theme.palette.secondary.main, fontSize: "28px" }}>{t('forgot_password')}</Typography>
                <Typography component="text" sx={{ color: theme.palette.grey['A700'], fontSize: "16px" }}>
                    {t('forgot_password_slogan')}
                </Typography>
            </Stack>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Stack rowGap={1}>
                    <TextField
                        fullWidth
                        label="Email Address"
                        {...register('email', { required: t('required_email'), pattern: { value: /^\S+@\S+$/i, message: t('invalid_email') } })}
                        error={!!errors.email}
                        helperText={errors.email?.message}
                        placeholder={t("email_placeholder")}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <IconButton>
                                        &#64;
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />
                    <CButton fullWidth disabled={!email} type="submit" borderRadius="lg" color="primary" text={t('submit')} />
                </Stack>
            </form>
        </Stack>
    )
}

type TForgetPasswordform = {
    email: string;
}