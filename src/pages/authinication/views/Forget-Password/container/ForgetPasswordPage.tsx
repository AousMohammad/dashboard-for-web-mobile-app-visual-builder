import React from 'react';
import { AuthContainer } from 'pages/authinication/container';
import { ForgetPasswordContent } from '../components';


export const ForgetPasswordPage: React.FC = () => {
    return (
        <AuthContainer AuthNewLeftSideContent={<ForgetPasswordContent />} url="forgetpassword" />
    )
}