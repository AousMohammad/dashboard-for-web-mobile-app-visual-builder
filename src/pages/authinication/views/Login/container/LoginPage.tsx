import React from 'react'

import { LoginForm } from '../components'
import { AuthContainer } from 'pages/authinication/container'

export const LoginPage: React.FC = () => {
    return (
        <AuthContainer AuthNewLeftSideContent={<LoginForm />} url="login" />
    )
}