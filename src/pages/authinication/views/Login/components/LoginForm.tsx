import React from 'react';

import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { TextField, FormControlLabel, Checkbox, Typography, Box, FormLabel, InputAdornment, IconButton, useTheme, useMediaQuery, FormControl, InputLabel } from '@mui/material';
import { useVisibilityPassword } from 'pages/authinication/hooks';
import { SocialAuthinication } from 'pages/authinication/components';
import CButton from 'components/CButton';

type FormValues = {
    email: string;
    password: string;
    remember: boolean;
};

export const LoginForm: React.FC = () => {
    const { passwordType } = useVisibilityPassword()

    const { register, watch, handleSubmit, formState: { errors } } = useForm<FormValues>();
    const { t } = useTranslation();
    const navigate = useNavigate();
    const onSubmit = (data: FormValues) => {
        console.log(data);
        // Perform your login action here
        navigate('/selection/type', { replace: true });
    };

    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));
    const [watchedEmail, watchedPassword] = watch(['email', 'password']);

    return (
        <div style={{ display: 'flex', flexDirection: 'column', position: 'relative', alignItems: 'center', width: '100%' }}>
            <SocialAuthinication AuthinicationType={'signin'} />
            <form onSubmit={handleSubmit(onSubmit)} style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
                <FormControl>
                    <InputLabel>
                        {t('email')}
                    </InputLabel>
                    <TextField
                        sx={{ mb: 3 }}
                        {...register('email', { required: t('required_email'), pattern: { value: /^\S+@\S+$/i, message: t('invalid_email') } })}
                        error={!!errors.email}
                        helperText={errors.email?.message}
                        placeholder={t("email_placeholder")}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <IconButton>
                                        &#64;
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />
                </FormControl>
                <FormControl>
                    <InputLabel>
                        {t('password')}
                    </InputLabel>
                    <TextField
                        type={passwordType}
                        sx={{ mb: 3 }}
                        {...register('password', { required: t('required_password') })}
                        error={!!errors.password}
                        helperText={errors.password?.message}
                        placeholder={t("password_placeholder")}
                    />
                </FormControl>
                <Box display='flex' justifyContent='space-between' alignItems='center' mb={matches ? 3 : 0}>
                    <FormControlLabel
                        control={<Checkbox {...register('remember')} />}
                        label={t('remember_me')}
                    />
                    <FormLabel sx={{ "&:hover": { cursor: 'pointer' } }} onClick={() => navigate('/forget-password')}>
                        {t('forgot_password')}
                    </FormLabel>
                </Box>
                <CButton disabled={!(watchedEmail && watchedPassword)} type="submit" borderRadius="lg" color="primary" text={t('login')} />
            </form>
            <Typography variant="body2" mt={matches ? 3 : 0}>{t('signup_message')} <a href="/signup">{t('signup')}</a></Typography>
        </div>
    );
};