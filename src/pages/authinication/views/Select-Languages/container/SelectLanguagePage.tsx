import React from 'react'

import { SelectLanguageContent } from "../components"
import { AuthContainer } from 'pages/authinication/container'


export const SelectLanguagePage: React.FC = () => {

    return (
        <AuthContainer AuthNewLeftSideContent={<SelectLanguageContent />} url="selectlanguage" />
    )
}