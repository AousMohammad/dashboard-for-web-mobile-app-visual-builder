import React from "react";
import { Box, Stack, Typography, useTheme } from "@mui/material";
import { useSelectLanguage } from "../hooks";
import { useNavigate } from "react-router-dom";
import { LanguageBtn, NextBtn } from "pages/authinication/components";

export const SelectLanguageContent: React.FC = () => {
    const navigate = useNavigate()
    const { selectedLanguage, onLanguageSelect } = useSelectLanguage();
    const nextBtnImgSrc = `${process.env.PUBLIC_URL}/assets/icons/chevron-right-${selectedLanguage ? "dark" : "gray"}.svg`;
    const theme = useTheme();
    return (
        <Stack rowGap={2}>
            <Stack mb="40px">
                <Typography component="text" textAlign="center" color={theme.palette.grey['A700']} fontSize="20px">حدد اللغة المناسبة لك</Typography>
                <Typography component="text" textAlign="center" color={theme.palette.secondary.main} fontSize="28px">Select Your Language</Typography>
            </Stack>
            <Box mb={4} display="flex" justifyItems="center" alignItems="center" columnGap={2}>
                <LanguageBtn isActive={selectedLanguage === "en"} onClick={onLanguageSelect} name="en">English</LanguageBtn>
                <LanguageBtn isActive={selectedLanguage === "ar"} onClick={onLanguageSelect} name="ar">العربية</LanguageBtn>
            </Box>
            <Box display="flex" justifyContent="center" alignItems="center">
                <NextBtn onClick={() => navigate("/login")} isActive={Boolean(selectedLanguage)} >
                    <img 
                        style={{ marginInlineEnd: '3%' }} 
                        src={nextBtnImgSrc} alt="Google Logo" 
                    />
                </NextBtn>
            </Box>
        </Stack>
    )
}