import React from "react";
import useAppSettings from "hooks/useAppSettings";


export const useSelectLanguage = () => {
    const { setAppLang } = useAppSettings();
    const [selectedLanguage, setSelectedLanguage] = React.useState("")
    const onLanguageSelect = React.useCallback((e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        const lang = e.currentTarget.name
        setSelectedLanguage(lang);
        setAppLang(lang)
    }, [setAppLang])

    return {
        selectedLanguage,
        onLanguageSelect
    }

}