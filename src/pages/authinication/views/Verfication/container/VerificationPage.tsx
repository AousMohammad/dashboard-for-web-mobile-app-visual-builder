import React from 'react'
import { VerifyForm } from '../components'
import { AuthContainer } from 'pages/authinication/container'


export const VerificationPage: React.FC = () => {

    return (
        <AuthContainer AuthNewLeftSideContent={<VerifyForm />} url="verifyaccount" />
    )
}