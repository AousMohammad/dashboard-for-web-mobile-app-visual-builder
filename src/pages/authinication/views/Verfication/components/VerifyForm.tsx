import React, { useState } from 'react'
import { Box, Drawer, Grid, Stack, TextField, Typography, useMediaQuery, useTheme } from '@mui/material';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import CButton from 'components/CButton';
type FormValues = {
    digits: string[];
};

export const VerifyForm: React.FC = () => {
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));
    const { t } = useTranslation();
    const {
        register,
        watch,
        handleSubmit,
        formState: { errors },
    } = useForm<FormValues>({
        defaultValues: { digits: Array(5).fill("") }
    });
    const navigate = useNavigate();
    const [open, setOpen] = useState(false);
    // const watchedCode = watch('verifyCode');
    const onSubmit = (data: FormValues) => {
        console.log(data);
        // Perform your login action here
        if (!matches)
            setOpen(true);
        else navigate('/login')
    };
    const [digits1, digits2, digits3, digits4, digits5, digits6] = watch(['digits.1', 'digits.2', 'digits.3', 'digits.4', 'digits.5', 'digits.6'])
    return (
        <div style={{ display: 'flex', flexDirection: 'column', position: 'relative', alignItems: 'center', width: '100%' }}>
            {matches && (
                <img src={process.env.PUBLIC_URL + '/assets/login.png'} className='circle' alt="Man holding a key" />
            )}
            {!matches && (
                <Drawer
                    anchor="bottom"
                    open={open}
                    sx={{ '& .MuiDrawer-paper': { padding: '10px', borderTopLeftRadius: '30px !important', borderTopRightRadius: '30px !important' } }}
                >
                    <Box height="40vh" display="flex" flexDirection="column" alignItems="center">
                        <img width='100%' src={process.env.PUBLIC_URL + '/assets/Confetti_L.png'} alt='Congratulations' />
                        <img width='20%' src={process.env.PUBLIC_URL + '/assets/facehappy.png'} alt='Happy face' />
                        <Typography variant='h1' mt={1} fontSize="15px" fontWeight="800">{t('thanks_joining')}</Typography>
                        <Typography textAlign="center" variant='body1' color="grey.A700" padding="4vw">{t('thanks_slogan')}</Typography>
                    </Box>
                    <CButton onClick={() => navigate('/login')} borderRadius="lg" color="primary" text={t('lets_start')} />
                </Drawer>
            )}
            <div className='formBG'>
                {matches && (
                    <Typography variant='body1' my={2} color="grey.A700" padding="2vw">{t('login_mobile_slogan')}</Typography>
                )}
                <form onSubmit={handleSubmit(onSubmit)} style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
                    <Box display="flex" justifyContent="space-between" margin="0 auto">
                        {Array.from({ length: 6 }, (_, i) => i).map(index => (
                            <TextField
                                {...register(`digits.${index + 1}`, {
                                    required: 'This field is required',
                                    pattern: {
                                        value: /^\d{1}$/,
                                        message: 'Only single digit allowed',
                                    },
                                })}
                                error={Boolean(errors.digits?.[index + 1])}
                                helperText={errors.digits?.[index + 1]?.message || " "}
                                type="text"
                                variant="outlined"
                                sx={{ marginInlineEnd: 2, borderRadius: '16px', '& input': { textAlign: 'center !important' } }}
                                inputProps={{ maxLength: 1 }}
                                key={index}
                            />
                        ))}
                    </Box>
                    <Grid item xs={12} md={6} justifyContent="flex-start" display="flex">
                        <Stack>
                            <Typography variant='body1' color="grey.A700" padding="0 2vw">{t('verify_not_recieved')}</Typography>
                            <Typography variant='body1' padding="2vw">{t('verify_resend')}</Typography>
                        </Stack>
                    </Grid>
                    <CButton disabled={!(digits1 && digits2 && digits3 && digits4 && digits5 && digits6)} type="submit" borderRadius="lg" color="primary" text={t('verfiy')} />
                </form>
            </div>
        </div>
    )
}