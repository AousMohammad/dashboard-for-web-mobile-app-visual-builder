import React from "react";

import { useTranslation } from "react-i18next";
import { AuthNewRightSide, AuthNewLeftSide } from "../components";
import { Box, Grid, Stack, Typography, useMediaQuery, useTheme } from "@mui/material";
import "styles/style.scss"
import { EcoAppBar } from "components/EcoAppBar";

/* This code exports a React functional component called `AuthContainer` that takes in two props:
`EcoAppBarRightContent` and `AuthNewLeftSideContent`. It also defines a TypeScript type called
`AAuthContainerProps` that specifies the types of these props. */
export const AuthContainer: React.FC<AAuthContainerProps> = ({ AuthNewLeftSideContent, EcoAppBarRightContent, url }) => {
  const theme = useTheme();
  const { t } = useTranslation()
  const matches = useMediaQuery(theme.breakpoints.up('md'))
  return (
    <Box width="100%" height="100%">
      <EcoAppBar children={EcoAppBarRightContent} />
      <Grid container spacing={2} height="90vh">
        {!matches && (
          <Grid item xs={12} md={6} justifyContent="flex-start" display="flex">
            <Stack>
              {/* <Typography variant='h4' fontWeight="800" paddingX="5vw">{t('login')}</Typography> */}
              <Typography variant='body1' color="grey.A700" padding="5vw">{t('login_mobile_slogan')}</Typography>
            </Stack>
          </Grid>
        )}
        <Grid order={matches ? 1 : 2} item xs={12} md={6} justifyContent="center" display="flex" alignItems="center">
          <AuthNewLeftSide>
            {AuthNewLeftSideContent}
          </AuthNewLeftSide>
        </Grid>
        <Grid order={matches ? 2 : 1} item xs={12} md={6} justifyContent="center" display="flex" alignItems="center">
          <AuthNewRightSide url={url} />
        </Grid>
      </Grid>
    </Box>
  );
}

type AAuthContainerProps = {
  EcoAppBarRightContent?: React.ReactNode;
  url: string;
  AuthNewLeftSideContent: React.ReactNode;
}