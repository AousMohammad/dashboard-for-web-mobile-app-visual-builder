import React from "react";
import { Visibility } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import VisibilityOffOutlinedIcon from '@mui/icons-material/VisibilityOffOutlined';


export const useVisibilityPassword = () => {
    const [showPassword, setShowPassword] = React.useState(false);
    const passwordType = showPassword ? "text" : "password";

    const VisibilityIcon = React.useCallback(() => (
        <IconButton onClick={() => setShowPassword(!showPassword)}>
            {showPassword ? <Visibility /> : <VisibilityOffOutlinedIcon />}
        </IconButton>
    ), [showPassword]);

    return {
        passwordType,
        VisibilityIcon
    }
}