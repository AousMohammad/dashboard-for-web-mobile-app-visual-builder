import React, { useRef, useState } from 'react'
import { Box } from '@mui/material'
import MusicPlayer from 'components/MusicPlayer';


type Props = {
    url: string;
}

export const AuthNewRightSide: React.FC<Props> = ({ url }) => {
    const [durationOfVideo, setDurationOfVideo] = useState(0);
    const [, setCurrentDurationOfVideo] = useState(0);

    const videoRef: any = useRef();


    const getDurationOfVideo = () => {

        const videoIntervalTime = setInterval(() => {

            setCurrentDurationOfVideo(parseFloat(videoRef.current.currentTime));

            if (parseFloat(videoRef.current.currentTime) >= durationOfVideo) {

                clearVideoInterval();
            }

        }, 1000)



        const clearVideoInterval = () => {
            clearInterval(videoIntervalTime);
        }

    }

    const videoReplay = () => {
        setDurationOfVideo(videoRef.current.duration);
        videoRef.current.currentTime = 0;
        videoRef.current.play();

        getDurationOfVideo();
    }
    const videoMute = () => {

        videoRef.current.muted = !videoRef.current.muted;
    }

    return (
        <Box sx={{
            width: '100%',
            height: '75%',
            display: 'flex',
            justifyContent: 'center',
            flexDirection: 'column',
            alignItems: 'center',
            position: 'relative'
        }}>
            <video width="100%" height="100%" ref={videoRef} autoPlay poster={process.env.PUBLIC_URL + `/assets/pooster.png`}>
                <source src={process.env.PUBLIC_URL + `/assets/animation/${url}.mp4`} type='video/mp4'></source>
            </video>
            <MusicPlayer videoPlay={videoReplay} videoMute={videoMute} />
        </Box>
    )
}