export { NextBtn } from "./NextBtn";
export { LanguageBtn } from "./LanguageBtn";
export { AuthNewRightSide } from "./AuthNewRightSide";
export { AuthNewLeftSide } from "./AuthNewLeftSide";
export { SocialAuthinication } from "./SocialAuthinication";