import React from "react";
import { useTranslation } from "react-i18next";
import { Typography, Stack, Divider, useTheme, useMediaQuery } from "@mui/material"
import CButton from "components/CButton";


export const SocialAuthinication: React.FC<TSocialAuthinicationProps> = ({ AuthinicationType }) => {
    const { t } = useTranslation();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'))

    return (
        <>
            {matches && (
                <>
                    <Typography my={3}>{t(`${AuthinicationType}_banner`)}</Typography>
                    <Typography color="InactiveCaptionText">{t(`${AuthinicationType}_description`)}</Typography>
                </>
            )}
            <Stack direction="row"
                justifyContent="space-between"
                alignItems="center"
                mb={1}
                width='100%'
                spacing={2}>
                <CButton fullWidth text={t(`${AuthinicationType}_google_auth`)} Icon={<img style={{ marginInlineEnd: '3%' }} src={process.env.PUBLIC_URL + '/assets/icons/google.png'} alt="Google Logo" />} borderRadius="lg" color="secondary" />
                <CButton fullWidth text={t(`${AuthinicationType}_apple_auth`)} Icon={<img style={{ marginInlineEnd: '3%' }} src={process.env.PUBLIC_URL + '/assets/icons/google.png'} alt="Google Logo" />} borderRadius="lg" color="secondary" />
            </Stack>
            <Divider sx={{ width: '100%', marginBottom: '8%' }}>{t('or')}</Divider>
        </>
    )
}

type TSocialAuthinicationProps = {
    AuthinicationType: "signin" | "signup"
}