import React from 'react'

export const AuthNewLeftSide: React.FC<React.PropsWithChildren> = ({ children }) => {
    return (
        <div className='centerer'>
            {children}
        </div>
    )
}