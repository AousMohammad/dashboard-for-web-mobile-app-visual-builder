import { styled, CSSObject } from '@mui/system';
import { Button, ButtonProps, useTheme } from '@mui/material';

interface TNextBtnProps extends ButtonProps {
    isActive: boolean
}

export const NextBtn = styled(Button)<TNextBtnProps>((props): CSSObject => {
    const theme = useTheme();
    return ({
        backgroundColor: props.isActive ? theme.palette.primary.main : theme.palette.grey['A100'],
        width: "70px",
        height: "70px",
        textAlign: "center",
        borderRadius: "35px",
        boxShadow: props.isActive ? theme.shadows?.[3] : theme.shadows?.[0],
        '&:hover': {
            backgroundColor: props.isActive ? theme.palette.primary.main : theme.palette.grey['A100'],
        },
    })
});
