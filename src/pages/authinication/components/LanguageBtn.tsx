import styled from '@emotion/styled';
import { Button, ButtonProps, useTheme } from '@mui/material';

interface TLanguageBtn extends ButtonProps {
    isActive: boolean
}
export const LanguageBtn = styled(Button)<TLanguageBtn>(({ isActive }: TLanguageBtn) => {
    const theme = useTheme();
    return ({
        color: isActive ? theme.palette.secondary.main : theme.palette.grey['A700'],
        backgroundColor: isActive ? theme.palette.primary.main : theme.palette.grey['A100'],
        width: "160px",
        height: "160px",
        padding: 0,
        overflow: 'hidden',
        textAlign: "center",
        borderRadius: "20px",
        fontWeight: isActive ? "bolder" : "normal",
        boxShadow: isActive ? theme.shadows?.[3] : theme.shadows?.[0],
        '&:hover': {
            color: isActive ? theme.palette.secondary.main : theme.palette.grey['A700'],
            backgroundColor: isActive ? theme.palette.primary.main : theme.palette.grey['A100'],
        },
    })
})
