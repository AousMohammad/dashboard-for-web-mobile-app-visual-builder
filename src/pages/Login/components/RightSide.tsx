import React from 'react'
import LoginForm from './LoginForm'

type Props = {}

const RightSide = (props: Props) => {
    return (
        <div className='centerer'>
            <LoginForm />
        </div>
    )
}

export default RightSide