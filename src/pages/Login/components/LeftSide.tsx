import React from 'react'
import { Box } from '@mui/material'
import { MusicPlayer } from '../../../components'

type Props = {}

const LeftSide = (props: Props) => {
    return (
        <Box sx={{
            width: '80%',
            height: '100%',
            display: 'flex',
            justifyContent: 'space-around',
            backgroundPosition: 'center',
            flexDirection: 'column',
            alignItems: 'center',
            backgroundSize: 'cover',
            backgroundImage: `url(${process.env.PUBLIC_URL + '/assets/rectangle.png'})`
        }}>
            <img src={process.env.PUBLIC_URL + '/assets/Smile.svg'} alt='Smile' />
            <MusicPlayer url={process.env.PUBLIC_URL + '/assets/Hello.mp3'} />
        </Box>
    )
}

export default LeftSide