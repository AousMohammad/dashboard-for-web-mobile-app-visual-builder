import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';

export default function CAppBar() {
    return (
        <Box sx={{ flexGrow: 1, height: '10vh' }}>
            <AppBar position="static" color='transparent'>
                <Toolbar sx={{ height: '8vh' }}>
                    <img src={process.env.PUBLIC_URL + '/assets/Logo.png'} alt='Over Zaki Logo' />
                </Toolbar>
            </AppBar>
        </Box>
    );
}