import React from 'react';
import { useForm } from 'react-hook-form';
import { Button, TextField, FormControlLabel, Checkbox, Divider, Typography, Stack, Box, FormLabel } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { Google, Apple } from '@mui/icons-material';
import { CButton } from '../../../components';

type FormValues = {
    email: string;
    password: string;
    remember: boolean;
};

const LoginForm: React.FC = () => {
    const { register, handleSubmit, formState: { errors } } = useForm<FormValues>();
    const { t } = useTranslation();
    const onSubmit = (data: FormValues) => {
        console.log(data);
        // Perform your login action here
    };

    return (
        <div style={{ display: 'flex', flexDirection: 'column', position: 'relative', alignItems: 'center', width: '100%' }}>
            <img src={process.env.PUBLIC_URL + '/assets/login.png'} className='circle' alt="Man holding a key" />
            <div className='formBG'>
                <Typography my={3}>{t('signin_banner')}</Typography>
                <Typography color="InactiveCaptionText">{t('signin_description')}</Typography>
                <Stack direction="row"
                    justifyContent="space-between"
                    alignItems="center"
                    mb={3}
                    width='100%'
                    spacing={2}>
                    <CButton fullWidth text={t('google_auth')} Icon={<Google sx={{ marginInlineEnd: 3 }} />} variant="contained" color="secondary" />
                    <CButton fullWidth text={t('apple_auth')} Icon={<Apple sx={{ marginInlineEnd: 3 }} />} variant="contained" color="secondary" />
                </Stack>
                <Divider sx={{ width: '100%', marginBottom: '3%' }}>{t('or')}</Divider>
                <form onSubmit={handleSubmit(onSubmit)} style={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
                    <TextField
                        label={t('email')}
                        sx={{ mb: 3 }}
                        {...register('email', { required: t('required_email'), pattern: { value: /^\S+@\S+$/i, message: t('invalid_email') } })}
                        error={!!errors.email}
                        helperText={errors.email?.message}
                    />
                    <TextField
                        label={t('password')}
                        type="password"
                        sx={{ mb: 3 }}
                        {...register('password', { required: t('required_password') })}
                        error={!!errors.password}
                        helperText={errors.password?.message}
                    />
                    <Box display='flex' justifyContent='space-between' alignItems='center' mb={3}>
                        <FormControlLabel
                            control={<Checkbox {...register('remember')} />}
                            label={t('remember_me')}
                        />
                        <FormLabel>
                            {t('forgot_password')}
                        </FormLabel>
                    </Box>
                    <Button type="submit" variant="contained" color="primary">{t('login')}</Button>
                </form>

                <Typography variant="body2" mt={3}>{t('signup_message')} <a href="/signup">{t('signup')}</a></Typography>
            </div>
        </div>
    );
};

export default LoginForm;
