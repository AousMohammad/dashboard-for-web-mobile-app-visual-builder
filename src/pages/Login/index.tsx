import React, { useState } from 'react';
import { useMutation } from 'react-query';
import { loginUser } from '../../services/authService';
import { TextField, Button, Typography, Box, Grid } from '@mui/material';
import { useTranslation } from 'react-i18next';
import './styles/style.scss';
import { CAppBar, LeftSide, RightSide } from './components';

const Login: React.FC = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const { t } = useTranslation();

  const mutation = useMutation(loginUser, {
    onSuccess: (data) => {
      // Handle user login here (e.g., set user state, store user data in local storage, etc.)
      console.log(data);
    },
  });

  const handleLogin = (event: React.FormEvent) => {
    event.preventDefault();
    mutation.mutate({ username, password });
  };

  return (
    <Box width="100%" height="100%">
      <CAppBar />
      <Grid container spacing={2} height="90vh">
        <Grid item xs={6} justifyContent="center" display="flex">
          <LeftSide />
        </Grid>
        <Grid item xs={6}>
          <RightSide />
        </Grid>
      </Grid>
    </Box>
  );
};

export default Login;
