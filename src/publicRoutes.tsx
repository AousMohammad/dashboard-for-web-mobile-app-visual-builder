import { PrivateRouteNoLayout } from "./components";
import {
    CategoryOfBusiness,
    ForgetPasswordPage,
    LoginPage,
    LogoOfBusiness,
    NameOfBusiness,
    ResetPasswordPage,
    SelectLanguagePage,
    SelectionPage,
    SignupPage,
    ThemeOfBusiness,
    TypeOfBusiness,
    VerificationPage
} from "./pages";
import { publicRoute } from "./types";

const publicRoutes: publicRoute[] = [
    {
        path: '/login',
        element: <LoginPage />,
    },
    {
        path: '/signup',
        element: <SignupPage />,
    },
    {
        path: '/select-lang',
        element: <SelectLanguagePage />,
    },
    {
        path: '/verify',
        element: <VerificationPage />,
    },
    {
        path: '/forget-password',
        element: <ForgetPasswordPage />,
    },
    {
        path: '/reset-password',
        element: <ResetPasswordPage />,
    },
    {
        path: '/selection',
        element: <PrivateRouteNoLayout element={<SelectionPage />} />,
        children: [
            {
                path: 'type',
                element: <TypeOfBusiness />
            },
            {
                path: 'category',
                element: <CategoryOfBusiness />
            },
            {
                path: 'name',
                element: <NameOfBusiness />
            },
            {
                path: 'logo',
                element: <LogoOfBusiness />
            },
            {
                path: 'theme',
                element: <ThemeOfBusiness />
            },
        ]
    },
];

export default publicRoutes;
