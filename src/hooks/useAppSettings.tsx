import React, { useState, useEffect } from 'react';
import Cookies from 'js-cookie';
import { AppSettings } from '../types';

const useAppSettings = (): AppSettings => {
    const [theme, setTheme] = useState<string>(Cookies.get('theme') || 'light');
    const [lang, setLang] = useState<string | undefined>(Cookies.get('i18next'));

    const setAppTheme = (newTheme: string): void => {
        Cookies.set('theme', newTheme);
        setTheme(newTheme);
    };

    const setAppLang = React.useCallback((newLang: string): void => {
        Cookies.set('i18next', newLang);
        setLang(newLang);
    }, []);

    // If you need to update the state when the cookie changes outside this hook
    useEffect(() => {
        const handleCookieChange = () => {
            setTheme(Cookies.get('theme') || 'light');
            setLang(Cookies.get('i18next') || 'en');
        };

        window.addEventListener('cookiechange', handleCookieChange);
        return () => {
            window.removeEventListener('cookiechange', handleCookieChange);
        };
    }, []);

    return { theme, setAppTheme, lang, setAppLang };
};

export default useAppSettings;
