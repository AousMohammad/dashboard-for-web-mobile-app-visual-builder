import { ReactElement } from "react";

export type Route = {
    path: string;
    element?: ReactElement;
    name?: string;
    icon?: ReactElement;
    children?: Route[];
}
export type publicRoute = {
    path: string;
    element: ReactElement;
    children?: publicRoute[];
}

export type AppSettings = {
    theme: string;
    lang: string | undefined;
    setAppTheme: (theme: string) => void;
    setAppLang: (lang: string) => void;
}

export type Tab = {
    tab: JSX.Element;
    title: string;
    icon: JSX.Element;
};