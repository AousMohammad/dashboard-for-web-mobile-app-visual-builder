import React, { ReactElement } from 'react'
import { Box, Typography } from '@mui/material'
import { ActionLink } from './ActionLink';

type Props = {
    children: ReactElement[];
}

export const ScrollerBox: React.FC<Props> = ({ children }) => {
    return (
        <Box sx={{
            width: '100%',

        }}>
            <Box sx={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
            }}>
                <Typography variant='h4'>test</Typography>
                <Typography variant='h4'>
                    <ActionLink url='dashboard' text='Test link' />
                </Typography>
            </Box>
            <Box sx={{
                display: '-webkit-box',
                width: '100%',
                overflowX: 'scroll'
            }}>
                {children.map((child, index) => child)}
            </Box>
        </Box>
    )
}