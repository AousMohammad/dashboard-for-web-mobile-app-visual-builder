import React, { useState, useEffect, ComponentType } from 'react';
import { Skeleton } from '@mui/material';

interface WithSkeletonProps {
    [key: string]: any;
}

function withSkeleton<P extends object>(WrappedComponent: ComponentType<P>) {
    const SkeletonWrapper: React.FC<P & WithSkeletonProps> = (props: P & WithSkeletonProps) => {
        const [loading, setLoading] = useState<boolean>(true);

        useEffect(() => {
            const timer = setTimeout(() => {
                setLoading(false);
            }, 5000);  // Here 2000ms is the loading time. You can adjust it.

            return () => clearTimeout(timer); // This will clear Timeout
        }, []);

        return (
            <>
                {loading
                    ? <Skeleton variant="rectangular" width={'100%'} height={'100%'} />
                    : <WrappedComponent {...props as P} />
                }
            </>
        )
    }

    return SkeletonWrapper;
}

export default withSkeleton;
