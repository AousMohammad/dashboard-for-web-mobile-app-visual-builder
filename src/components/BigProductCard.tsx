import { Box, Card, Chip, Grid, Typography } from '@mui/material'
import React from 'react'


type Props = {
    index: number;
    imgUrl: string;
    name: string;
    type: string;
    number: number;
}

type TProps = {
    data: Props;
}

export const BigProductCard = (props: TProps) => {
    const { number, imgUrl, name, type, index } = props.data;
    return (
        <Card variant='white'>
            <Grid container spacing={1} alignItems='center'>
                <Grid item xs={1}>
                    <Chip variant='completed' label={`#${index}`} />
                </Grid>
                <Grid item xs={1}>
                    <Box sx={{ borderRadius: '15px', width: '60px', height: '60px', border: '1px solid grey', overflow: 'hidden' }}>

                    </Box>
                    {imgUrl}
                </Grid>
                <Grid item xs={4}>
                    <Typography variant='h4'>&lt;BigProductCard /&gt;</Typography>
                    <Typography variant='subtitle1'>{type}</Typography>
                </Grid>
                <Grid item xs={2}>
                    <Typography variant='h4'>{name}</Typography>
                    <Typography variant='subtitle1'>{type}</Typography>
                </Grid>
                <Grid item xs={2}>
                    <Typography variant='h4'>{name}</Typography>
                    <Typography variant='subtitle1'>{type}</Typography>
                </Grid>
                <Grid item xs={2}>
                    <Typography variant='h4'>{name}</Typography>
                    <Typography variant='subtitle1'>{type}</Typography>
                </Grid>
            </Grid>
        </Card>
    )
}