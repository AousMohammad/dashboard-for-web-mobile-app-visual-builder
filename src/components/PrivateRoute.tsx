import React, { useEffect } from 'react';
import { Outlet, useNavigate } from 'react-router-dom';
import { useAppSettings } from '../hooks';
import { Layout } from 'pages/dashboard';


const PrivateRoute: React.FC = () => {
  const navigate = useNavigate();
  const isAuthenticated = true; // Fetch your authentication status from your state management system
  const { lang } = useAppSettings();
  useEffect(() => {
    if (!isAuthenticated && lang) {
      navigate('/login');
    } else if (!isAuthenticated && !lang) {
      navigate('/select-lang');
    }
    // eslint-disable-next-line
  }, [isAuthenticated, navigate]);

  if (!isAuthenticated) {
    return null;
  }

  return (
    <Layout>
      <Outlet />
    </Layout>
  );
};

export default PrivateRoute;
