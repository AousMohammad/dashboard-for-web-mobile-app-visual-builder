import React from "react";
import { ButtonProps, BoxProps, Button, Box } from "@mui/material";


export const ButtonGroupWhite: React.FC<TBtns> = ({ btns,  boxProps}) => {
    const [state, setState] = React.useState(0);
    const variantBtn = (ind: number) => ind === state ? "white" : "whiteDisabled";
    const renderBtns = () => btns.map((btn, ind) => {
        const onClick: ButtonProps["onClick"] = (e) => {
            setState(ind);
            btn?.onClick?.(e)
        }
        return (
            <Button {...btn} variant={variantBtn(ind)} onClick={onClick} >
                {btn.title}
            </Button>
        )
    })
    return (
        <Box
            display="flex"
            justifyContent="space-evenly"
            alignItems="center"
            columnGap={2}
            sx={(theme) => ({
                borderRadius: "16px",
                backgroundColor: theme.palette.grey?.["A400"],
            })}
            {...boxProps}
        >
            {
                renderBtns()
            }
        </Box>
    )
}

type TBtns = {
    btns: Array<ButtonProps>,
    boxProps?: BoxProps
} 