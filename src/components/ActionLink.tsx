import { ChevronRight } from '@mui/icons-material';
import { Button } from '@mui/material'
import React from 'react'
import { useNavigate } from 'react-router-dom';

type Props = {
    url: string;
    text: string;
    withIcon?: boolean;
}

export const ActionLink: React.FC<Props> = ({ url, text, withIcon = false }) => {
    const navigate = useNavigate();
    return (
        <Button
            sx={{
                color: 'black',
                backgroundColor: 'transparent',
            }}
            onClick={() => navigate(url, { replace: true })}
        >
            {text}
            {withIcon && (
                <ChevronRight />
            )}
        </Button>
    )
}