import React from 'react';
import { Fab, Stack, useMediaQuery, useTheme } from '@mui/material';

type props = {
    videoPlay: () => void;
    videoMute: () => void;
}

const MusicPlayer: React.FC<props> = ({ videoPlay, videoMute }) => {
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'))

    return (
        <>
            {
                !matches ?
                    <>
                        <Fab color="secondary" onClick={videoPlay} sx={{ position: 'absolute', left: '14vw', top: '3vh' }}>
                            <img src={process.env.PUBLIC_URL + "/assets/icons/repeat.svg"} alt="repeat icon" />
                        </Fab>
                        <Fab color="secondary" onClick={videoMute} sx={{ position: 'absolute', right: '14vw', top: '3vh' }}>
                            <img src={process.env.PUBLIC_URL + "/assets/icons/mute.svg"} alt="mute icon" />
                        </Fab>
                    </>
                    :
                    <>
                        <Stack direction="row"
                            justifyContent="space-around"
                            alignItems="center"
                            width="90%"
                            spacing={2}>
                            <Fab color="secondary" onClick={videoPlay}>
                                <img src={process.env.PUBLIC_URL + "/assets/icons/repeat.svg"} alt="repeat icon" />
                            </Fab>
                            <Fab color="secondary" onClick={videoMute}>
                                <img src={process.env.PUBLIC_URL + "/assets/icons/mute.svg"} alt="mute icon" />
                            </Fab>
                        </Stack>
                    </>
            }
        </>
    );
};

export default MusicPlayer;
