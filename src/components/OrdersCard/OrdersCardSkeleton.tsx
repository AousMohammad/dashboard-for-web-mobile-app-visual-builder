import React from 'react'
import { Flag } from '@mui/icons-material'
import { Card, Chip, Grid, Skeleton, Stack, Typography } from '@mui/material'

type TProps = {
    id: number;
    status: "completed";
    number: number;
    price: number;
    date: any;
    country: string;
    name: string;
}

type Props = {
    data: TProps;
}

export const OrdersCardSkeleton = (props: Props) => {
    const {
        id,
        status,
        number,
        price,
        date,
        country,
        name
    } = props.data;

    return (
        <Card variant='white'>
            <Grid container spacing={1} className='center'>
                <Grid item xs={6}>
                    <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
                    <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
                    <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
                    <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
                </Grid>
                <Grid item xs={6}>
                    <Stack direction='column' alignItems='flex-end'>
                        <Skeleton variant="rectangular" width={100} height={10} sx={{ mb: 2 }} />
                        <Skeleton variant="rectangular" width={50} height={2} sx={{ mb: 2}} />
                        <Skeleton variant="rectangular" width={50} height={2} sx={{ mb: 2 }} />
                    </Stack>
                </Grid>
            </Grid>
        </Card>
    )
}