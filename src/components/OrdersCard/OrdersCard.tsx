import React from 'react'
import { Flag } from '@mui/icons-material'
import { Card, Chip, Grid, Stack, Typography } from '@mui/material'

type TProps = {
    id: number;
    status: "completed";
    number: number;
    price: number;
    date: any;
    country: string;
    name: string;
}

type Props = {
    data: TProps;
}

export const OrdersCard = (props: Props) => {
    const {
        id,
        status,
        number,
        price,
        date,
        country,
        name
    } = props.data;

    return (
        <Card variant='white'>
            <Grid container spacing={1} className='center'>
                <Grid item xs={6}>
                    <Typography style={{ marginBottom: '10px' }} variant='body1'>#{id}</Typography>
                    <Typography style={{ marginBottom: '10px' }} variant='subtitle1'>{date}</Typography>
                    <Typography variant='body1' style={{ marginBottom: '10px' }}>&lt;OrdersCard /&gt;</Typography>  
                    <Stack direction='row' alignItems="center">
                        <span style={{ marginInlineEnd: '10px' }}>{country}</span>
                        <Typography variant='h4'>{name}</Typography>
                    </Stack>
                </Grid>
                <Grid item xs={6}>
                    <Stack direction='column' alignItems='flex-end'>
                        <Chip variant={status} label={status} style={{ marginBottom: '10px' }} />
                        <Typography variant='body1' style={{ marginBottom: '10px' }}>{number} items</Typography>
                        <Typography variant='body1' style={{ marginBottom: '10px' }}>{price} KWD</Typography>
                    </Stack>
                </Grid>
            </Grid>
        </Card>
    )
}