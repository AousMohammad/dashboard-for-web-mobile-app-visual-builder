import React, { useContext } from 'react';
import { ThemeContext } from '../contexts';


const MyComponent = () => {
    const theme = useContext(ThemeContext);
    const { dark, toggle } = theme;

    const themeStyles = {
        backgroundColor: dark ? 'black' : 'white',
        color: dark ? 'white' : 'black',
    };

    return (
        <div style={themeStyles}>
            <h1>This is a {dark ? 'dark' : 'light'} themed component</h1>
            <button onClick={toggle}>Switch Theme</button>
        </div>
    );
};

export default MyComponent;
