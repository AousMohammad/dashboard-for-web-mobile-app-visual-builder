import { Box, Button, Card, Chip, Stack, Typography } from '@mui/material'
import React from 'react'

type Props = {}

export const TaskCard = (props: Props) => {
    return (
        <Card variant='white'>
            <Stack direction='row' mb={3}>
                <Box width='50%'>
                    <Typography variant='body1'>test</Typography>
                    <Typography variant='body1'>test</Typography>
                </Box>
                <Box width='50%' display='flex' justifyContent='flex-end'>
                    <Chip variant='completed' label='completed' />
                </Box>
            </Stack>
            <Box width='100%' mb={3}>
                <Typography variant='darkB' sx={{ mb: 3 }}>Title</Typography>
                <Typography variant='grey' sx={{ mt: 2, overflowًrap: 'break-word' }}>body1body1body1body1body1body1body1body1 body1body1body1body1body1body1</Typography>
            </Box>
            <Box>
                <Button variant='contained' sx={{ width: '40%' }}>Button</Button>
            </Box>
        </Card >
    )
}