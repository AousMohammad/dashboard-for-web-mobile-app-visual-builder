import React, { useState } from "react";
import { Knob, KnobChangeEvent } from 'primereact/knob';
import { useTranslation } from "react-i18next";

export const KnobProgress: React.FC = () => {
    const [value, setValue] = useState<number>(55);
    const { t } = useTranslation();
    return (
        <div className="flex justify-content-center">
            <Knob
                min={0}
                max={365}
                valueTemplate={`{value} ${t('days_left')}`}
                size={150}
                value={value}
                onChange={(e: KnobChangeEvent) => setValue(e.value)}
            />
        </div>
    )
}