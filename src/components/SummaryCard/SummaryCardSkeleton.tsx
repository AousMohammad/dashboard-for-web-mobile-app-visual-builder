import { Avatar, Card, Grid, Skeleton, Stack, Typography } from '@mui/material'
import React from 'react'

type Props = {
    title: string;
    number: number;
    icon: React.ReactElement;
}

export const SummaryCardSkeleton: React.FC<Props> = ({ title, number, icon }) => {
    return (
        <Card variant='white'>
            <Grid container spacing={1} className='center'>
                <Grid item xs={2}>
                    <Skeleton variant="circular" width={40} height={40} />
                </Grid>
                <Grid item xs={9}>
                    <Stack>
                        <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
                        <Skeleton variant="rectangular" width={210} height={60} />
                    </Stack>
                </Grid>
            </Grid>
        </Card>
    )
}