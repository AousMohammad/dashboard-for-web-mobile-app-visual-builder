import { Avatar, Card, Grid, Stack, Typography } from '@mui/material'
import React from 'react'

type Props = {
    title: string;
    number: number;
    icon: React.ReactElement;
}

export const SummaryCard: React.FC<Props> = ({ title, number, icon }) => {
    return (
        <Card variant='white'>
            <Grid container spacing={1} className='center'>
                <Grid item xs={2}>
                    <Avatar variant='greyB'>
                        {icon}
                    </Avatar>
                </Grid>
                <Grid item xs={9}>
                    <Stack>
                        <Typography variant='subtitle1'>{title}</Typography>
                        <Typography variant='h4'>{title}</Typography>
                        <Typography variant='h4'>&lt;SummaryCard /&gt;</Typography>
                    </Stack>
                </Grid>
            </Grid>
        </Card>
    )
}