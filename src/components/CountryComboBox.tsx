import React from 'react';

import TextField from '@mui/material/TextField';
import CircularProgress from '@mui/material/CircularProgress';

import { useQuery } from 'react-query';
import { Autocomplete, Box, FormControl, InputLabel } from '@mui/material';
import { FormState, UseFormRegister } from 'react-hook-form';
import { fetchCountries } from 'services/fetchCountries';
import { useTranslation } from 'react-i18next';
import { FormValues } from 'pages/authinication/views/Signup/components/SignupForm';

interface Country {
  name: string;
  flag: string;
}

export const CountryComboBox: React.FC<TCountryComboBoxProps> = ({ register, formState: { errors } }) => {
  const [isOpen, setOpen] = React.useState(false)
  const { data: countries, isLoading, isError } = useQuery<Country[]>('countries', fetchCountries, {
    enabled: isOpen, // Initially disable the query
  });
  const { t } = useTranslation();

  return (
    <Autocomplete
      sx={{ mb: 3 }}
      {...register('country')}
      fullWidth
      multiple={false}
      options={countries || []}
      getOptionLabel={(country) => country.name}
      loading={isLoading}
      loadingText="Loading countries..."
      noOptionsText={isError ? 'Error loading countries' : 'No countries found'}
      renderOption={(props, option) => (
        <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
          <img
            src={option.flag}
            alt={option.name}
            style={{ marginRight: '8px' }}
            width="20"
          />
          {option.name}
        </Box>
      )}
      onOpen={() => setOpen(true)}
      renderInput={(params) => (
        <FormControl>
          <InputLabel>
            {t('select_country')}
          </InputLabel>
          <TextField
            {...params}
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <>
                  {isLoading ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                  {params.InputProps.endAdornment}
                </>
              ),
            }}
          />
        </FormControl>
      )}
    />
  );
};

type TCountryComboBoxProps = {
  register: UseFormRegister<FormValues>,
  formState: FormState<FormValues>
}
