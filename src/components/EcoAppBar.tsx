import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import { useTheme } from '@mui/material';

export const EcoAppBar: React.FC<React.PropsWithChildren> = ({ children }) => {
    const theme = useTheme();
    return (
        <Box sx={{ flexGrow: 1, height: '10vh' }}>
            <AppBar position="static" color='transparent'>
                <Toolbar sx={{ height: '8vh' }}>
                    <img style={{ filter: theme.palette.mode === 'dark' ? 'invert(1)' : ''}} src={process.env.PUBLIC_URL + '/assets/Logo.png'} alt='Over Zaki Logo' />
                </Toolbar>
                {children}
            </AppBar>
        </Box>
    );
}