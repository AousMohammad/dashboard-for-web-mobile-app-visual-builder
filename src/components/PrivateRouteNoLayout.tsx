import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppSettings } from '../hooks';

interface PrivateRouteProps {
    element: React.ReactElement;
}

const PrivateRouteNoLayout: React.FC<PrivateRouteProps> = ({ element }) => {
    const navigate = useNavigate();
    const isAuthenticated = true; // Fetch your authentication status from your state management system
    const { lang } = useAppSettings();
    useEffect(() => {
        if (!isAuthenticated && lang) {
            navigate('/login');
        } else if (!isAuthenticated && !lang) {
            navigate('/select-lang');
        }
        // eslint-disable-next-line
    }, [isAuthenticated, navigate]);

    if (!isAuthenticated) {
        return null;
    }

    return (
        <>
            {element}
        </>
    );
};

export default PrivateRouteNoLayout;
