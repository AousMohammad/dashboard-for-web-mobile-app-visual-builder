import * as React from 'react';
import { CssBaseline, Box, Toolbar, List, Typography, Divider, IconButton, Badge, Container, ListItem, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';

import { Menu, ChevronLeft, Notifications } from '@mui/icons-material';
import routes from '../../routes';
import { useTranslation } from 'react-i18next';
import { getDirection } from '../../utils';
import { AppBar, Drawer } from './Layout';

interface LayoutProps {
    children?: React.ReactNode;
}




const Layout: React.FC<LayoutProps> = ({ children }) => {
    const [open, setOpen] = React.useState(true);
    const toggleDrawer = () => {
        setOpen(!open);
    };
    const { t } = useTranslation();
    const direction = getDirection();
    return (
        <Box sx={{
            display: 'flex', '& header': {
                marginLeft: direction === 'ltr' ? '240px' : '0 !important',
                left: direction === 'ltr' ? 'auto' : '0 !important',
                right: direction === 'ltr' ? '0' : 'auto !important',
            }
        }}>
            <CssBaseline />
            <AppBar position="absolute" open={open}>
                <Toolbar
                    sx={{
                        pr: '24px', // keep right padding when drawer closed
                    }}
                >
                    <IconButton
                        edge="start"
                        color="inherit"
                        aria-label="open drawer"
                        onClick={toggleDrawer}
                        sx={{
                            marginRight: '36px',
                            ...(open && { display: 'none' }),
                        }}
                    >
                        <Menu />
                    </IconButton>
                    <Typography
                        component="h1"
                        variant="h6"
                        color="inherit"
                        noWrap
                        sx={{ flexGrow: 1 }}
                    >
                        Dashboard
                    </Typography>
                    <IconButton color="inherit">
                        <Badge badgeContent={4} color="secondary">
                            <Notifications />
                        </Badge>
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Drawer variant="permanent" open={open}>
                <Toolbar
                    sx={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                        px: [1],
                    }}
                >
                    <IconButton onClick={toggleDrawer}>
                        <ChevronLeft />
                    </IconButton>
                </Toolbar>
                <Divider />
                <List component="nav">
                    {routes.map((route) => (
                        <>
                            <ListItem key={t(route.name)} disablePadding>
                                <ListItemButton>
                                    <ListItemIcon>
                                        {route.icon}
                                    </ListItemIcon>
                                    <ListItemText primary={t(route.name)} />
                                </ListItemButton>
                            </ListItem>
                            <Divider sx={{ my: 1 }} />
                        </>
                    ))}
                </List>
            </Drawer>
            <Box
                component="main"
                sx={{
                    backgroundColor: (theme) =>
                        theme.palette.mode === 'light'
                            ? theme.palette.grey[100]
                            : theme.palette.grey[900],
                    flexGrow: 1,
                    height: '100vh',
                    overflow: 'auto',
                }}
            >
                <Toolbar />
                <Container maxWidth="xl" sx={{ mt: 4, mb: 4 }}>
                    {children}
                </Container>
            </Box>
        </Box>
    );
}

export default Layout;