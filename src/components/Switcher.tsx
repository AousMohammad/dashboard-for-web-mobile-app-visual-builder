import { Button, ButtonGroup } from '@mui/material'
import React from 'react'

type Props = {}

export const Switcher = (props: Props) => {
    return (
        <ButtonGroup
            disableElevation
            variant="contained"
            aria-label="Disabled elevation buttons"
        >
            <Button>One</Button>
            <Button>Two</Button>
        </ButtonGroup>
    )
}