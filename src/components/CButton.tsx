import React from 'react';
import { Button, ButtonProps } from '@mui/material';

type BorderType = 'sm' | 'md' | 'lg';

interface CustomButtonProps extends ButtonProps {
  text: string;
  Icon?: JSX.Element;
  borderRadius?: BorderType;
}

const CButton: React.FC<CustomButtonProps> = ({
  text,
  Icon,
  borderRadius = 'sm',
  variant = 'contained',
  ...rest
}) => {
  const borderRadiusMap = {
    sm: '10px',
    md: '20px',
    lg: '30px',
  };

  return (
    <Button
      variant={variant}
      sx={{
        borderRadius: borderRadiusMap[borderRadius],
        paddingTop: 2,
        paddingBottom: 2,
        marginY: 2,
      }}
      {...rest}
    >
      {Icon}
      {text}
    </Button>
  );
};

export default CButton;
