import React from 'react';
import { useLocation } from 'react-router-dom';
import { ActionLink } from './ActionLink';
import routes from '../routes';
import _ from 'lodash';

const findRouteName = (path: string) => {
    let routeName = '';
    const pathArray = path.split('/');

    const findName = (routes: any[], pathArray: string[], index: number = 0) => {
        for (let i = 0; i < routes.length; i++) {
            if (routes[i].path === `/${pathArray[index]}`) {
                routeName = routes[i].name;
                if (index < pathArray.length - 1 && routes[i].children) {
                    findName(routes[i].children, pathArray, index + 1);
                }
                break;
            }
        }
    };

    findName(routes, pathArray);

    return routeName;
};

export const BreadCrumb: React.FC = () => {
    const location = useLocation();
    const pathnames = location.pathname.split('/').filter((x) => x);

    // Skip the first element (i.e., 'dashboard')
    return (
        <>
            {pathnames.slice(1).map((path, index) => {
                const breadcrumbPath = `/${pathnames.slice(0, index + 2).join('/')}`;
                const routeName = findRouteName(breadcrumbPath);

                return (
                    <span key={_.uniqueId()}>
                        {index > 0 && ' / '}
                        <ActionLink text={routeName || path} url={breadcrumbPath} />
                    </span>
                );
            })}
        </>
    );
};
