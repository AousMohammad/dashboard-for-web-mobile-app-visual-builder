import React, { useContext, useEffect, useState } from 'react';
import { BrowserRouter as Router, Routes, Route, Outlet } from 'react-router-dom';
import { StyledEngineProvider, ThemeProvider as MuiThemeProvider } from '@mui/material/styles';
import privateRoutes from './routes';
import publicRoutes from './publicRoutes';
import PrivateRoute from './components/PrivateRoute';
import _ from 'lodash';
import { lightTheme, darkTheme } from './theme';
import ThemeContext, { ThemeProvider } from './contexts/ThemeContext';
import LangContext, { LangProvider } from 'contexts/LangContext';

const renderRoutes = (routes: any) => {
  return routes.map((route: any) => {
    const { path, element, children } = route;
    return (
      <Route key={_.uniqueId()} path={path} element={element || <><Outlet /></>}>
        {children && renderRoutes(children)}
      </Route>
    );
  });
};

const AppRoutes: React.FC = () => {
  const themeContext = useContext(ThemeContext);
  const langContext = useContext(LangContext);

  const [direction, setDirection] = useState(langContext.lang === 'ar' ? 'rtl' : 'ltr');

  useEffect(() => {
    setDirection(langContext.lang === 'ar' ? 'rtl' : 'ltr');
  }, [langContext.lang]);

  const theme = themeContext.dark
    ? { ...darkTheme, direction: direction }
    : { ...lightTheme, direction: direction };

  // Switch the dir attribute for the whole app
  useEffect(() => {
    document.body.dir = direction;
  }, [direction]);

  return (
    <StyledEngineProvider injectFirst>
      <MuiThemeProvider theme={theme}>
        <Router>
          <Routes>
            {publicRoutes.map((route) => (
              <Route key={_.uniqueId()} path={route.path} element={route.element}>
                {route.children?.map(childRoute => (
                  <Route key={_.uniqueId()} path={childRoute.path} element={childRoute.element} />
                ))}
              </Route>
            ))}
            {/* {privateRoutes.map((route) => (
              <Route key={_.uniqueId()} path={route.path} element={<PrivateRoute />}>
                {route.children?.map(childRoute => (
                  <Route key={_.uniqueId()} path={childRoute.path} element={childRoute.element} />
                ))}
              </Route>
            ))} */}
            {renderRoutes(privateRoutes)}
          </Routes>
        </Router>
      </MuiThemeProvider>
    </StyledEngineProvider>
  );
}

function App() {
  return (
    <ThemeProvider>
      <LangProvider>
        <AppRoutes />
      </LangProvider>
    </ThemeProvider>
  );
}

export default App;
