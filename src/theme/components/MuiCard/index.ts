import { Components, Shadows } from "@mui/material";
import { Palette } from '@mui/material/styles';

const common = {
    padding: '30px',
    height: '160px',
  }

  const smallCommon = {
    borderRadius: '20px',
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    "& p": {
      marginTop: "10px"
    },
    "& svg": {
      width: "30px",
      height:  "32px"
    }
    
  }

  const smallCards = {
    width: '50px',
    height: '50px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '12px',
    padding: 10,
    filter: 'drop-shadow(0px 0px 6px #00000022)'
  }

  export const MuiCard = (palette: Palette, shadows?: Shadows): Components["MuiCard"] => ({
    styleOverrides: {
        root: {
          color: palette.primary.main,
          boxShadow: shadows?.[1],
          padding: common.padding,
          margin: 10,
        }
      },
      variants: [
        {
          props: { variant: 'white' },
          style: {
            color: palette?.common.black,
            backgroundColor: palette.common.white,
          },
        },
        {
          props: { variant: 'primary' },
          style: {
            color: palette.common.black,
            backgroundColor: palette.primary.main,
          },
        },
        {
          props: { variant: 'grey' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: `${palette.grey['A100']} 0% 0% no-repeat padding-box`,
            "svg path": {
              fill: palette.grey["A700"]
            }
          },
        },
        {
          props: { variant: 'red' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: palette.error.opacity,
            "svg path": {
              fill: palette.error
            }
          },
        },
        {
          props: { variant: 'orange' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: palette.warning.opacity,
            "svg path": {
              fill: palette.warning.main
            },
            "& p": {
              marginTop: "10px"
            }
          },
        },
        {
          props: { variant: 'pink' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: palette.pinkOP,
            "svg path": {
              fill: palette.pink
            }
          },
        },
        {
          props: { variant: 'brown' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: palette.brownOP,
            "svg path": {
              fill: palette.brown
            }
          },
        },
        {
          props: { variant: 'blue' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: palette.blueOP,
            "svg path": {
              fill: palette.blue
            }
          },
        },
        {
          props: { variant: 'lightBlue' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: palette.lightBlueOP,
            "svg path": {
              fill: palette.lightBlue
            }
          },
        },
        {
          props: { variant: 'purple' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: palette.purpleOP,
            "svg path": {
              fill: palette.purple
            }
          },
        },
        {
          props: { variant: 'green' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: palette.greenOP,
            "svg path": {
              fill: palette.green
            }
          },
        },
        {
          props: { variant: 'iron' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: palette.ironOP,
            "svg path": {
              fill: palette.iron
            }
          },
        },
        {
          props: { variant: 'springGreen' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: palette.springGreenOP,
            "svg path": {
              fill: palette.springGreen
            }
          },
        },
        {
          props: { variant: 'electricBlue' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: palette.electricBlueOP,
            "svg path": {
              fill: palette.electricBlue
            }
          },
        },
        {
          props: { variant: 'goldenrod' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: palette.goldenrodOP,
            "svg path": {
              fill: palette.goldenrod,
            },
          },
        },
        {
          props: { variant: 'smallLightGrey' },
          style: {
            ...smallCards,
            backgroundColor: palette.grey['A100'],
          }
        },
        {
          props: { variant: 'smallPrimary' },
          style: {
            ...smallCards,
            backgroundColor: palette.primary.main,
          }
        }
      ],
})