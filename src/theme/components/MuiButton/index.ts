import { Components, Palette } from "@mui/material";

const commonButton = {
    borderRadius: '20px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '10px 20px'
}

export const MuiButton = (palette: Palette): Components["MuiButton"] => ({
    styleOverrides: {
        root: {
          margin: '10px'
        }
      },
      defaultProps: {
        color: "primary",
        variant: "contained",
        disableElevation: true,
        disableRipple: false,
        sx: (theme) => ({
          textTransform: "none",
        })
      },
      variants: [
        {
          props: { color: "primary"},
          style: {
            borderRadius: "30px",
            boxShadow: "0px 0px 20px 5px #2222221a",
            color: palette.secondary.main,
            font: "normal normal 900 18px/33px Avenir Arabic",
            ":hover": {
              backgroundColor: palette.primary.main
            },
            "& svg path": {
              fill: palette.secondary.main,
            }
          }
        },
        {
          props: { color: "secondary"},
          style: {
            borderRadius: "30px",
            color: palette.grey?.["A700"],
            font: "normal normal 900 18px/33px Avenir Arabic",
            backgroundColor: palette.grey?.["A400"],
            ":hover": {
              backgroundColor: palette.grey?.["A400"],
            },
            "& svg path": {
              fill: palette.grey?.["A700"],
            }
          }
        },
        {
          props: { color: "info"},
          style: {
            font: "normal normal 900 14px/33px Avenir Arabic",
            borderRadius: "20px",
            color: palette.common.white,
            backgroundColor: palette.secondary.main,
            boxShadow: "0px 6px 20px #00000033",
            ":hover": {
              backgroundColor: palette.secondary.main,
            },
            "& svg path": {
              fill: palette.common.white,
            }
          }
        },
        {
          props: { color: "error"},
          style: {
            font: "normal normal 900 18px/33px Avenir Arabic",
            borderRadius: "30px",
            color: palette.error.main,
            backgroundColor: palette.error.opacity,
            boxShadow: "0px 6px 20px #1BFCB633",
            ":hover": {
              backgroundColor: palette.error.opacity,
            },
            "& svg path": {
              fill: palette.error.main,
            }
          }
        },
        {
          props: { color: "success"},
          style: {
            font: "normal normal 900 14px/33px Avenir Arabic",
            borderRadius: "30px",
            color: palette.secondary.main,
            backgroundColor: palette.primary.opacity,
            boxShadow: "0px 6px 20px #1BFCB633",
            ":hover": {
              backgroundColor: palette.primary.opacity,
            },
            "& svg path": {
              fill: palette.secondary.main,
            }
          }
        },
        {
          props: { variant: "textLightGrey"},
          style: {
            font: "normal normal 900 16px/33px Avenir Arabic",
            color: palette.grey?.["A700"],
            boxShadow: "none",
            backgroundColor: palette.grey?.["A100"],
            ":hover": {
              backgroundColor: palette.grey?.["A100"],
            },
            "& svg path": {
              fill: palette.grey?.["A700"],
            }
          }
        },
        {
          props: { variant: "textDarkGrey"},
          style: {
            font: "normal normal 900 16px/33px Avenir Arabic",
            color: palette.secondary.main,
            boxShadow: "none",
            backgroundColor: palette.grey?.["A100"],
            ":hover": {
              backgroundColor: palette.grey?.["A100"],
            },
            "& svg path": {
              fill: palette.secondary.main,
            }
          }
        },
        {
          props: { variant: "white"},
          style: {
            borderRadius: "12px",
            minHeight: "30px",
            // margin: "5px",
            // padding: "3px 22px",
            minWidth: "100px",
            font: "normal normal 900 16px/33px Avenir Arabic",
            color: palette.secondary.main,
            boxShadow: "0px 6px 20px #00000033",
            backgroundColor: palette.common?.white,
            ":hover": {
              backgroundColor: palette.common?.white,
            },
            "& svg path": {
              fill: palette.secondary.main,
            }
          }
        },
        {
          props: { variant: "whiteDisabled"},
          style: {
            minHeight: "30px",
            borderRadius: "12px",
            minWidth: "100px",
            color: palette.grey?.["A700"],
            boxShadow: "none",
            font: "normal normal 900 16px/33px Avenir Arabic",
            ":hover": {
              backgroundColor: "transparent",
            },
            "& svg path": {
              fill: palette.grey?.["A700"],
            }
          }
        },
        {
         props: { variant: 'grad' },
          style: {
            ...commonButton,
            color: palette.secondary.main,
            backgroundColor: 'transparent',
            background: 'linear-gradient(270deg, #1BFCB6 0%, #0DE5FD 100%)'
          }
        },
        {
         props: { variant: 'dark' },
          style: {
            ...commonButton,
            color: palette.common.white,
            backgroundColor: palette.secondary.main
          }
        },
        {
         props: { variant: 'light' },
          style: {
            ...commonButton,
            color: palette.secondary.main,
            backgroundColor: palette.primary.opacity
          }
        },
        {
         props: { variant: 'grey' },
          style: {
            ...commonButton,
            color: palette.grey['A700'],
            backgroundColor: palette.grey['A100'],
          }
        },
      ]
})