import { Components, Palette } from "@mui/material";


export const MuiAvatar = (palette: Palette): Components["MuiAvatar"] => ({
    variants: [{
        props: { variant: 'grey' },
        style: {
          width: '28px',
          height: '28px',
          backgroundColor: `${palette.iron}`
        }
      }, {
        props: { variant: 'greyB' },
        style: {
          width: '50px',
          height: '50px',
          backgroundColor: `${palette.ironOP}`
        }
      }]
})