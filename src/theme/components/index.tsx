import { MuiCard } from "./MuiCard";
import { MuiChip } from "./MuiChip";
import { MuiInput } from "./MuiInput";
import { MuiButton } from "./MuiButton";
import { MuiAvatar } from "./MuiAvatar";
import { MuiTextField } from "./MuiTextField";
import { MuiTypography } from "./MuiTypography";
import { MuiInputLabel } from "./MuiInputLabel";
import { MuiFormControl } from "./MuiFormControl";
import { MuiAutocomplete } from "./MuiAutocomplete";
import { MuiInputAdornment } from "./MuiInputAdornment";
import { Palette, Shadows, ThemeOptions } from "@mui/material";

export const components = (palette: Palette, shadows?: Shadows): ThemeOptions["components"] => ({
    MuiChip: MuiChip(palette),
    MuiCard: MuiCard(palette),
    MuiInput: MuiInput(palette),
    MuiButton: MuiButton(palette),
    MuiAvatar: MuiAvatar(palette),
    MuiTextField: MuiTextField(palette),
    MuiTypography: MuiTypography(palette),
    MuiInputLabel: MuiInputLabel(palette),
    MuiFormControl: MuiFormControl(palette),
    MuiAutocomplete: MuiAutocomplete(palette),
    MuiInputAdornment: MuiInputAdornment(palette),
})