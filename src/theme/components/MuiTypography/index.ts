import { Components, Palette } from "@mui/material";


export const MuiTypography = (palette: Palette): Components["MuiTypography"] => {
  console.log(palette)
    return {variants: [{
        props: { variant: 'grey' },
        style: {
          color: palette.grey['A700'],
          display: 'block'
        }
    }, {
        props: { variant: 'dark' },
        style: {
          color: palette.secondary.main,
        }
    }, {
        props: { variant: 'darkB' },
        style: {
          color: palette.secondary.main,
          fontWeight: '800'
        }
    }, {
        props: { variant: "headerDashboard"},
        style: {
          color: palette.secondary.main,
        }
    }]}
}