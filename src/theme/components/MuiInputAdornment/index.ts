import { Components, Palette } from "@mui/material";


export const MuiInputAdornment = (palette: Palette): Components["MuiInputAdornment"] => ({
    styleOverrides: {
        root: {
          background: 'transparent',
          backgroundColor: 'transparent !important',
        }
    }
})