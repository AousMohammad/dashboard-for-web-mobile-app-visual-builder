import { Components, Palette } from "@mui/material";


export const MuiFormControl = (palette: Palette): Components["MuiFormControl"] => ({
    styleOverrides: {
        root: {
          color: 'red',
          width: '100%',
          marginBottom: '15px'
        }
      },
      defaultProps: {
        variant: 'standard'
      }
})