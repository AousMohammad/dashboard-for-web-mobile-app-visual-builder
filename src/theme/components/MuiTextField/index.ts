import { Components, Palette } from "@mui/material";


export const MuiTextField = (palette: Palette): Components["MuiTextField"] => ({
    defaultProps: {
        variant: 'standard',
    },
    styleOverrides: {
        root: {
          "& div": {
            backgroundColor: '#F0F0F4',
            border: '0px solid transparent',
            position: 'relative',
          },
        }
    },
})