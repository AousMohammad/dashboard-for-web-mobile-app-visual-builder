import { Components, Palette } from "@mui/material/styles";


export const MuiChip = (palette: Palette): Components["MuiChip"] => ({
  variants: [{
    props: { variant: 'completed' },
    style: {
      backgroundColor: palette.primary.main,
      color: palette.secondary.main
    }
  }]
})