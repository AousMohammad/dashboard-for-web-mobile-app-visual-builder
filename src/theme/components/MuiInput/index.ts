import { Components, Palette } from "@mui/material";


export const MuiInput = (palette: Palette): Components["MuiInput"] => ({
    styleOverrides: {
        root: {
          height: '50px',
          padding: '5px 10px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        },
        underline: {
          "&:before": {
            borderBottom: "none",
          },
          "&:after": {
            borderBottom: "none",
          },
          "&:hover:not(.Mui-disabled):before": {
            borderBottom: "none",
          },
        },
      },
})