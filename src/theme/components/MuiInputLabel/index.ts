import { Components, Palette } from "@mui/material";


export const MuiInputLabel = (palette: Palette): Components["MuiInputLabel"] => ({
    defaultProps: {
        shrink: true,
    },
    styleOverrides: {
        root: {
            top: '-20px',
            left: '5px'
        }
    }
})