import { Components, Palette } from "@mui/material";


export const MuiAutocomplete = (palette: Palette): Components["MuiAutocomplete"] => ({
    styleOverrides: {
        root: {
          "div:nth-child(2)": {
            background: 'transparent',
            top: 'calc(50% - 2.5vh)'
          }
        },
      }
})