import { Palette, createTheme } from "@mui/material/styles";
import { paletteDark } from "./palette"
import { typography } from "theme/typography";
import { components } from "theme/components";
import { shape } from "theme/shape";
import { zIndex } from "theme/zIndex";
import { darkShadows } from "./shadows";
import { mixins } from "theme/mixins";

export const darkTheme = createTheme({
    palette: {
        mode: "dark",
        ...paletteDark
    },
    shape: shape,
    zIndex: zIndex,
    typography: typography(paletteDark),
    mixins: mixins(paletteDark as Palette, darkShadows(paletteDark as Palette)),
    shadows: darkShadows(paletteDark as Palette),
    components: components(paletteDark as Palette)
})