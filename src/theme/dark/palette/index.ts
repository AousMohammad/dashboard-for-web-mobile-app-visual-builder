import { PaletteOptions } from '@mui/material/styles/createPalette';

export const paletteDark: PaletteOptions = {
    primary: {
      main: '#19FCB5',
      opacity: '#19FCB52D',
    },
    secondary: {
      main: '#0F1349',
    },
    info: {
      main: '#21CEff'
    },
    success: {
      main: '#21CE99'
    },
    warning: {
      main: '#FB8500',
      opacity: '#FB86002D'
    },
    error: {
      main: '#FF008B',
      opacity: '#FF008C2D'
    },
    grey: {
      "A700": '#8688A3',
      "A100": '#F5F5F8'
    },
    springGreen: "#8DC73F",
    springGreenOP: "#8DC73F2D",
    electricBlue: "#4CC9F0",
    electricBlueOP: "#4CC9F02D",
    goldenrod: "#EFCA08",
    goldenrodOP: "#EFCA082D",
    brown: '#B5838D',
    brownOP: '#B5838D2D',
    pink: '#EA84C9',
    pinkOP: '#EA84C92D',
    blue: '#0466C8',
    blueOP: '#0466C82D',
    lightBlue: '#2196F3',
    lightBlueOP: '#2196F32D',
    purple: '#D54CFF',
    purpleOP: '#D54CFF2D',
    green: '#02C39A',
    greenOP: '#02C39A2D',
    iron: '#FF5D8F',
    ironOP: '#FF5D8F2D',
    common: {
      white: '#FFFFFF',
      black: '#222222'
    }
  };