import { ThemeOptions, Palette } from "@mui/material/styles";

export const darkShadows = (palette: Palette) => {
    return [
        "none", //0
        // '0px 0px 20px #00000014', // 1
        // 'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
        // "0px 6px 20px rgba(27, 252, 182, 0.30)", // 3
        // `0px 4px 20px ${palette.secondary.main}`, // DashboardTopNav boxShadow for paperProps 4
        // `0px 4px 20px ${palette.common.black}`,   // DashboardTopNav boxshadow for image 5
        // '0px 0px 5px 0px black', // 6
        // "0px -6px 40px #00000014", // 7
        ...Array.from({ length: 24}, () => "")
    ] as ThemeOptions["shadows"]
}