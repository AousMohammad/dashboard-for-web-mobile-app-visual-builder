import { shape } from "theme/shape";
import { lightShadows } from "./shadows";
import { components } from "theme/components";
import { typography } from "theme/typography";
import { lightPalette } from "./palette"
import { Palette, createTheme } from "@mui/material";
import { zIndex } from "theme/zIndex";
import { mixins } from "theme/mixins";

export const lightTheme = createTheme({
    palette: {
        mode: "light",
        ...lightPalette,
    },
    shape: shape,
    zIndex: zIndex,
    mixins: mixins(lightPalette as Palette, lightShadows(lightPalette as Palette)),
    typography: typography(lightPalette),
    shadows: lightShadows(lightPalette as Palette),
    components: components(lightPalette as Palette, lightShadows(lightPalette as Palette))
})