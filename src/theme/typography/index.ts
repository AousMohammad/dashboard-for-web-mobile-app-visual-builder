import { PaletteOptions, ThemeOptions } from "@mui/material/styles";

export const typography = (palette: PaletteOptions): ThemeOptions['typography'] => {
  return {
    fontFamily: 'Main, sans-serif',
    h1: {
      fontWeight: '800',
      color: palette.common?.black,
      fontSize: '24px'
    },
    h2: {
      fontWeight: '700',
      color: palette.common?.black,
      fontSize: '19px'
    },
    h3: {
      fontWeight: '600',
      color: palette.common?.black,
      fontSize: '18px'
    },
    h4: {
      fontWeight: '600',
      color: palette.common?.black,
      fontSize: '16px'
    },
    h5: {
      fontWeight: '500',
      color: palette.common?.black,
      fontSize: '14px'
    },
    h6: {
      fontWeight: '500',
      color: palette.common?.black,
      fontSize: '12px'
    },
    body1: {
      fontWeight: '400',
      color: palette.grey?.['A700'],
      fontSize: '16px'
    },
    body2: {
      fontWeight: '400',
      color: palette.common?.black,
      fontSize: '14px'
    },
    subtitle1: {
      fontWeight: '400',
      color: palette.common?.black,
      fontSize: '12px'
    }
}}