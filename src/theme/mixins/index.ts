import { Mixins, Palette, Shadows } from "@mui/material/styles";

export const mixins = (palette: Palette, shadows?: Shadows): Mixins => ({
    flexCenter: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    borderBox(width: number, color: string, borderStyle?: string) {
        return {
            boxSizing: 'border-box',
            borderWidth: `${width}px`,
            borderColor: color,
            borderStyle: borderStyle || 'solid',
        }
    },
    toolsCard: (bgColo: string, svgColor: string, borderRadius?: string) => ({
        flexDirection: "column",
        backgroundColor: bgColo,
        borderRadius: borderRadius || '20px',
        width: '100%',
        height: '100%',
        "svg path": {
            fill: svgColor
        },
        "& p": {
            marginTop: "10px"
        }
    }),
    greyWhiteBtn: (state: boolean) => {
        const common: React.CSSProperties = {
            borderRadius: "12px",
            font: "normal normal 900 16px/33px Avenir Arabic",
            textAlign: "center",
        }
        const whiteBtnStyle: React.CSSProperties = {
            ...common,
            backgroundColor: palette.common.white,
            color: palette.secondary.main,
            boxShadow: "0px 6px 20px #00000033"
        };

        const greyBtnStyle: React.CSSProperties = {
            ...common,
            backgroundColor: palette.grey?.["A400"],
            color: palette.grey?.["A700"],
            boxShadow: "none"
        }

        return state ? whiteBtnStyle : greyBtnStyle
    }
})