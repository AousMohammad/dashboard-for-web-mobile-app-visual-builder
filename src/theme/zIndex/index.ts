import { ThemeOptions } from "@mui/material"

export const zIndex: ThemeOptions["zIndex"] = {
    modal: 1300,
    drawer: 1100,
    appBar: 1200,
    tooltip: 1500,
    snackbar: 1400,
    speedDial: 1050,
    mobileStepper: 1000,
}


