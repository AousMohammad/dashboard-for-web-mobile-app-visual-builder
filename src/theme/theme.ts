import { Theme, createTheme } from '@mui/material/styles';

interface CustomTheme extends Theme {
  customPalette: {
    brown: string;
    brownOP: string;
    pink: string;
    pinkOP: string;
    blue: string;
    blueOP: string;
    lightBlue: string;
    lightBlueOP: string;
    purple: string;
    purpleOP: string;
    green: string;
    greenOP: string;
    iron: string;
    ironOP: string;
  };

}

const smallCommon = {
  borderRadius: '20px',
  width: '100%',
  height: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  "& p": {
    marginTop: "10px"
  },
  "& svg": {
    width: "30px",
    height:  "32px"
  }
  
}

const common = {
  fontFamily: 'Main, sans-serif',
  borderRadius: '16px',
  padding: '30px',
  height: '160px',
  boxShadow: '0px 0px 20px #00000014',
}

const commonButton = {
  borderRadius: '20px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  padding: '10px 20px'
}

export const paletteLight = {
  primary: {
    main: '#19FCB5',
    opacity: '#19FCB52D',
  },
  secondary: {
    main: '#0F1349',
  },
  info: {
    main: '#21CEff'
  },
  success: {
    main: '#21CE99'
  },
  warning: {
    main: '#FB8500',
    opacity: '#FB86002D'
  },
  error: {
    main: '#FF008B',
    opacity: '#FF008C2D'
  },
  grey: {
    "A700": '#8688A3',
    "A100": '#F5F5F8'
  },
  springGreen: "#8DC73F",
  springGreenOP: "#8DC73F2D",
  electricBlue: "#4CC9F0",
  electricBlueOP: "#4CC9F02D",
  goldenrod: "#EFCA08",
  goldenrodOP: "#EFCA082D",
  brown: '#B5838D',
  brownOP: '#B5838D2D',
  pink: '#EA84C9',
  pinkOP: '#EA84C92D',
  blue: '#0466C8',
  blueOP: '#0466C82D',
  lightBlue: '#2196F3',
  lightBlueOP: '#2196F32D',
  purple: '#D54CFF',
  purpleOP: '#D54CFF2D',
  green: '#02C39A',
  greenOP: '#02C39A2D',
  iron: '#FF5D8F',
  ironOP: '#FF5D8F2D',
  common: {
    white: '#FFFFFF',
    black: '#222222'
  }
};

export const paletteDark = {
  primary: {
    main: '#19FCB5',
    opacity: '#19FCB52D',
  },
  secondary: {
    main: '#0F1349',
  },
  info: {
    main: '#21CEff'
  },
  success: {
    main: '#21CE99'
  },
  warning: {
    main: '#FB8500',
    opacity: '#FB86002D'
  },
  error: {
    main: '#FF008B',
    opacity: '#FF008C2D'
  },
  grey: {
    "A700": '#8688A3',
    "A100": '#F5F5F8'
  },
  springGreen: "#8DC73F",
  springGreenOP: "#8DC73F2D",
  electricBlue: "#4CC9F0",
  electricBlueOP: "#4CC9F02D",
  goldenrod: "#EFCA08",
  goldenrodOP: "#EFCA082D",
  brown: '#B5838D',
  brownOP: '#B5838D2D',
  pink: '#EA84C9',
  pinkOP: '#EA84C92D',
  blue: '#0466C8',
  blueOP: '#0466C82D',
  lightBlue: '#2196F3',
  lightBlueOP: '#2196F32D',
  purple: '#D54CFF',
  purpleOP: '#D54CFF2D',
  green: '#02C39A',
  greenOP: '#02C39A2D',
  iron: '#FF5D8F',
  ironOP: '#FF5D8F2D',
  common: {
    white: '#FFFFFF',
    black: '#222222'
  }
};

const lightTheme = createTheme({
  palette: {
    mode: 'light',
    ...paletteLight
  },
  typography: {
    fontFamily: common.fontFamily,
    h1: {
      fontWeight: '800',
      color: paletteLight.common.black,
      fontSize: '20px'
    },
    h2: {
      fontWeight: '700',
      color: paletteLight.common.black,
      fontSize: '19px'
    },
    h3: {
      fontWeight: '600',
      color: paletteLight.common.black,
      fontSize: '18px'
    },
    h4: {
      fontWeight: '600',
      color: paletteLight.common.black,
      fontSize: '16px'
    },
    h5: {
      fontWeight: '500',
      color: paletteLight.common.black,
      fontSize: '14px'
    },
    h6: {
      fontWeight: '500',
      color: paletteLight.common.black,
      fontSize: '12px'
    },
    body1: {
      fontWeight: '400',
      color: paletteLight.grey['A700'],
      fontSize: '16px'
    },
    body2: {
      fontWeight: '400',
      color: paletteLight.common.black,
      fontSize: '14px'
    },
    subtitle1: {
      fontWeight: '400',
      color: paletteLight.common.black,
      fontSize: '12px'
    }
  },
  components: {
    MuiFormControl: {
      styleOverrides: {
        root: {
          color: 'red',
          width: '100%',
          marginBottom: '15px'
        }
      },
      defaultProps: {
        variant: 'standard'
      }
    },
    MuiInputLabel: {
      defaultProps: {
        shrink: true,
      },
      styleOverrides: {
        root: {
            top: '-20px',
            left: '5px'
        }
      }
    },
    MuiTextField: {
      defaultProps: {
        variant: 'standard',
      },
      styleOverrides: {
        root: {
          "& div": {
            backgroundColor: '#F0F0F4',
            border: '0px solid transparent',
            position: 'relative',
            borderRadius: '16px',
          },
        }
      },
    },
    MuiInput: {
      styleOverrides: {
        root: {
          height: '50px',
          padding: '5px 10px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        },
        underline: {
          "&:before": {
            borderBottom: "none",
          },
          "&:after": {
            borderBottom: "none",
          },
          "&:hover:not(.Mui-disabled):before": {
            borderBottom: "none",
          },
        },
      },
    },
    MuiAutocomplete: {
      styleOverrides: {
        root: {
          "div:nth-child(2)": {
            background: 'transparent',
            top: 'calc(50% - 2.5vh)'
          }
        },
      }
    },
    MuiInputAdornment: {
      styleOverrides: {
        root: {
          background: 'transparent',
          backgroundColor: 'transparent !important',
        }
      }
    },
    MuiCard: {
      styleOverrides: {
        root: {
          color: paletteLight.secondary.main,
          borderRadius: common.borderRadius,
          boxShadow: common.boxShadow,
          padding: common.padding,
          margin: 10,
        }
      },
      variants: [
        {
          props: { variant: "accountSetting"},
          style: {
            height: "100px",
            backgroundColor: paletteLight.common.white,
            boxShadow: "0px -6px 40px #00000014",
            borderRadius: "20px",
            cursor: "pointer"
          }
        },
        {
          props: { variant: 'white' },
          style: {
            color: paletteLight.common.black,
            backgroundColor: paletteLight.common.white,
          },
        },
        {
          props: { variant: 'primary' },
          style: {
            color: paletteLight.common.black,
            backgroundColor: paletteLight.primary.main,
          },
        },
        {
          props: { variant: 'grey' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: `${paletteLight.grey['A100']} 0% 0% no-repeat padding-box`,
            "svg path": {
              fill: paletteLight.grey["A700"]
            }
          },
        },
        {
          props: { variant: 'red' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: paletteLight.error.opacity,
            "svg path": {
              fill: paletteLight.error
            }
          },
        },
        {
          props: { variant: 'orange' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: paletteLight.warning.opacity,
            "svg path": {
              fill: paletteLight.warning.main
            },
            "& p": {
              marginTop: "10px"
            }
          },
        },
        {
          props: { variant: 'pink' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: paletteLight.pinkOP,
            "svg path": {
              fill: paletteLight.pink
            }
          },
        },
        {
          props: { variant: 'brown' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: paletteLight.brownOP,
            "svg path": {
              fill: paletteLight.brown
            }
          },
        },
        {
          props: { variant: 'blue' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: paletteLight.blueOP,
            "svg path": {
              fill: paletteLight.blue
            }
          },
        },
        {
          props: { variant: 'lightBlue' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: paletteLight.lightBlueOP,
            "svg path": {
              fill: paletteLight.lightBlue
            }
          },
        },
        {
          props: { variant: 'purple' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: paletteLight.purpleOP,
            "svg path": {
              fill: paletteLight.purple
            }
          },
        },
        {
          props: { variant: 'green' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: paletteLight.greenOP,
            "svg path": {
              fill: paletteLight.green
            }
          },
        },
        {
          props: { variant: 'iron' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: paletteLight.ironOP,
            "svg path": {
              fill: paletteLight.iron
            }
          },
        },
        {
          props: { variant: 'springGreen' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: paletteLight.springGreenOP,
            "svg path": {
              fill: paletteLight.springGreen
            }
          },
        },
        {
          props: { variant: 'electricBlue' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: paletteLight.electricBlueOP,
            "svg path": {
              fill: paletteLight.electricBlue
            }
          },
        },
        {
          props: { variant: 'goldenrod' },
          style: {
            ...smallCommon,
            flexDirection: "column",
            backgroundColor: paletteLight.goldenrodOP,
            "svg path": {
              fill: paletteLight.goldenrod,
            },
          },
        },
      ],
    },
    MuiChip: {
      variants: [{
        props: { variant: 'completed' },
        style: {
          backgroundColor: paletteLight.primary.main,
          color: paletteLight.secondary.main
        }
      }]
    },
    MuiButton: {
      styleOverrides: {
        root: {
          margin: '10px'
        }
      },
      variants: [
        {
         props: { variant: 'grad' },
          style: {
            ...commonButton,
            color: paletteLight.secondary.main,
            backgroundColor: 'transparent',
            background: 'linear-gradient(270deg, #1BFCB6 0%, #0DE5FD 100%)'
          }
        },
        {
         props: { variant: 'dark' },
          style: {
            ...commonButton,
            color: paletteLight.common.white,
            backgroundColor: paletteLight.secondary.main
          }
        },
        {
         props: { variant: 'light' },
          style: {
            ...commonButton,
            color: paletteLight.secondary.main,
            backgroundColor: paletteLight.primary.opacity
          }
        },
        {
         props: { variant: 'grey' },
          style: {
            ...commonButton,
            color: paletteLight.grey['A700'],
            backgroundColor: paletteLight.grey['A100'],
          }
        },
      ]
    },
    MuiTypography: {
      variants: [{
        props: { variant: 'grey' },
        style: {
          color: paletteLight.grey['A700'],
          display: 'block'
        }
      }, {
        props: { variant: 'dark' },
        style: {
          color: paletteLight.secondary.main,
        }
      }, {
        props: { variant: 'darkB' },
        style: {
          color: paletteLight.secondary.main,
          fontWeight: '800'
        }
      }, {
        props: { variant: "headerDashboard"},
        style: {
          color: paletteLight.secondary.main,
        }
      }]
    },
    MuiAvatar: {
      variants: [{
        props: { variant: 'grey' },
        style: {
          width: '28px',
          height: '28px',
          backgroundColor: `${paletteLight.ironOP}`
        }
      }, {
        props: { variant: 'greyB' },
        style: {
          width: '50px',
          height: '50px',
          backgroundColor: `${paletteLight.ironOP}`
        }
      }]
    }
}}) as CustomTheme;

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
    ...paletteDark
  },
  typography: {
    fontFamily: common.fontFamily,
    allVariants: {
      color: paletteDark.secondary.main,
    }
  },
  components: {
    MuiCard: {
      styleOverrides: {
        root: {
          color: paletteDark.secondary.main,
          borderRadius: common.borderRadius,
          boxShadow: common.boxShadow,
          padding: common.padding,
        }
      },
      variants: [
        {
          props: { variant: 'white' },
          style: {
            color: paletteDark.common.black,
            backgroundColor: paletteDark.common.white,
          },
        },
        {
          props: { variant: 'primary' },
          style: {
            color: paletteDark.common.black,
            backgroundColor: paletteDark.primary.main,
          },
        },
        {
          props: { variant: 'grey' },
          style: {
            ...smallCommon,
            backgroundColor: `${paletteDark.grey['A100']} 0% 0% no-repeat padding-box`,
          },
        },
        {
          props: { variant: 'red' },
          style: {
            ...smallCommon,
            backgroundColor: paletteDark.error.opacity,
          },
        },
        {
          props: { variant: 'orange' },
          style: {
            ...smallCommon,
            backgroundColor: paletteDark.warning.opacity,
          },
        },
        {
          props: { variant: 'pink' },
          style: {
            ...smallCommon,
            backgroundColor: paletteDark.pinkOP,
          },
        },
        {
          props: { variant: 'brown' },
          style: {
            ...smallCommon,
            backgroundColor: paletteDark.brownOP,
          },
        },
        {
          props: { variant: 'blue' },
          style: {
            ...smallCommon,
            backgroundColor: paletteDark.blueOP,
          },
        },
        {
          props: { variant: 'lightBlue' },
          style: {
            ...smallCommon,
            backgroundColor: paletteDark.lightBlueOP,
          },
        },
        {
          props: { variant: 'purple' },
          style: {
            ...smallCommon,
            backgroundColor: paletteDark.purpleOP,
          },
        },
        {
          props: { variant: 'green' },
          style: {
            ...smallCommon,
            backgroundColor: paletteDark.greenOP,
          },
        },
        {
          props: { variant: 'iron' },
          style: {
            ...smallCommon,
            backgroundColor: paletteDark.ironOP,
          },
        },
      ],
    },
    MuiButton: {
      styleOverrides: {
        root: {
          margin: '10px'
        }
      },
      variants: [
        {
         props: { variant: 'grad' },
          style: {
            ...commonButton,
            color: paletteLight.secondary.main,
            backgroundColor: 'transparent',
            background: 'linear-gradient(270deg, #1BFCB6 0%, #0DE5FD 100%)'
          }
        },
        {
         props: { variant: 'dark' },
          style: {
            ...commonButton,
            color: paletteLight.common.white,
            backgroundColor: paletteLight.secondary.main
          }
        },
        {
         props: { variant: 'light' },
          style: {
            ...commonButton,
            color: paletteLight.secondary.main,
            backgroundColor: paletteLight.primary.opacity
          }
        },
        {
         props: { variant: 'grey' },
          style: {
            ...commonButton,
            color: paletteLight.grey['A700'],
            backgroundColor: paletteLight.grey['A100'],
          }
        },
      ]
    },
  }
});

export { lightTheme, darkTheme };
