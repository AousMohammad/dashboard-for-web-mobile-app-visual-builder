import React, { ReactElement, useCallback, useMemo } from "react";
import { useAppSettings } from "hooks";

type Props = {
    children: ReactElement;
}

interface LangContextValue {
    lang: string;
    setLang: (newLang: string) => void;
}

const defaultLangState: LangContextValue = {
    lang: 'en',
    setLang: () => { },
};

const LangContext = React.createContext<LangContextValue>(defaultLangState);

export const LangProvider: React.FC<Props> = ({ children }) => {
    const { lang: appLang = 'en', setAppLang } = useAppSettings();
    const [langState, setLangState] = React.useState(appLang);

    const setLang = useCallback((newLang: string) => {
        setAppLang(newLang);
        setLangState(newLang);
    }, [setAppLang]);

    const value = useMemo(() => ({ lang: langState, setLang }), [langState, setLang]);

    return (
        <LangContext.Provider value={value}>
            {children}
        </LangContext.Provider>
    );
};



export default LangContext;
