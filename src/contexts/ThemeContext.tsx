import React, { useMemo, useCallback } from 'react';
import { useAppSettings } from 'hooks';

interface ThemeContextValue {
  dark: boolean;
  toggle: () => void;
}

const defaultState: ThemeContextValue = {
  dark: false,
  toggle: () => { },
};

const ThemeContext = React.createContext<ThemeContextValue>(defaultState);

export const ThemeProvider: React.FC<any> = ({ children }) => {
  const { theme, setAppTheme } = useAppSettings();
  const [dark, setDark] = React.useState(theme === 'dark' ? true : false);

  const toggle = useCallback(() => {
    setDark((prevDark) => {
      const newTheme = prevDark ? 'light' : 'dark';
      setAppTheme(newTheme);
      return !prevDark;
    });
  }, [setAppTheme]);

  const contextValue = useMemo(() => {
    return { dark, toggle };
  }, [dark, toggle]);

  return (
    <ThemeContext.Provider value={contextValue}>
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeContext;
