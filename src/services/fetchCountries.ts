interface Country {
    name: string;
    flag: string;
  }

export const fetchCountries = async (): Promise<Country[]> => {
    const response = await fetch('https://restcountries.com/v2/all');
    if (!response.ok) {
      throw new Error('Failed to fetch countries');
    }
    const data = await response.json();
    return data as Country[];
  };