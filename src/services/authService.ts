import axios from 'axios';

interface LoginProps {
  username: string;
  password: string;
}

export const loginUser = async ({ username, password }: LoginProps) => {
  const { data } = await axios.post('https://my-api-url/login', {
    username,
    password,
  });
  return data;
};
