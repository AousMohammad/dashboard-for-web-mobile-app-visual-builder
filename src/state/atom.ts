import { atom } from "recoil";

export const deviceState = atom({
  key: 'device',
  default: 'phone'

});

export const activeTab = atom({
  key: 'activeTab',
  default: 0,
});



export const mockUp = atom({
    key: 'mockup',
    default: {
      style: {
        font: 'Arial',
        button: {
          borderRadius: 0
        },
        logo: 'url',
        colors: {
          primary: 'black',
          secondary: 'white'
        },
        cartIcon: 'url',
        card: '1',

      },
      layout: {
        navbar: {
          order: 1,
          style: 'left'
        },
        home: {
          banner: {
            enabled: true,
            order: 2,
            images: ['url1', 'url2']
          },
          header: {
            enabled: true,
            order: 3,
            image: 'url',
            slogan: 'text'
          },
          categories: {
            enabled: true,
            order: 4,
            list: 'row'
          },
          products: {
            enabled: true,
            order: 5,
            list: 'grid',
          },
          customSections: [{
            order: 6,
            type: 'deals',
            name: 'deals',
            arabic_name: 'اعلان',
            list: 'grid',
          }],
        },
        categories: {
          list: 'grid1',
          style: 'first',
          shape: 'rounded'
        },
        product: {
          list: 'slider',
          description: true,
          similar_products: true,
          wishlist: true,

        },
        products: {
          list: 'grid1',
          search: true,
          filter: {
            enabled: true,
            type: 'tags'
          },
          card: {
            wishlist: true,
            description: true,
            category: true,
            add_to_cart: true,
            style: 'card1'
          }
        },
        signup: {
            full_name: true,
            mobile_number: true,
            birth_date: false,
            gender: true,
            social_media: {
              google: true,
              facebook: true,
              twitter: true
            }
          },
      },
    },
  });