export { ReactComponent as Home } from './home.svg';
export { ReactComponent as Orders } from './orders.svg';
export { ReactComponent as Categories } from './Categories.svg';
export { ReactComponent as Products } from './Products.svg';
export { ReactComponent as Customers } from './customers.svg';
export { ReactComponent as Analytics } from './Analytics.svg';
export { ReactComponent as Payment } from './Payment.svg';
export { ReactComponent as Vouchers } from './Vouchers.svg';
export { ReactComponent as Settings } from './Settings.svg';
export { ReactComponent as DeliveryPickup } from './Delivery-Pickup.svg';
export { ReactComponent as Integrations } from './Integrations.svg';
export { ReactComponent as EditProfile } from './Edit-profile.svg';
export { ReactComponent as MyWallet } from './wallet.svg';
export { ReactComponent as MySubscription } from './bolt-solid-status.svg';
export { ReactComponent as MyTasks } from './tasks.svg';
export { ReactComponent as AboutOverZaki } from './circle-info-solid.svg';
export { ReactComponent as HelpSupport } from './Help.svg';
export { ReactComponent as ContactUs } from './contact.svg';
export { ReactComponent as DarkMode } from './dark.svg';
export { ReactComponent as Logout } from './logout.svg';
export { ReactComponent as Website } from './websites.svg';
export { ReactComponent as Design } from "./Design.svg"
export { ReactComponent as Domain } from "./Domain.svg"
// accountSetting
export { ReactComponent as AccountSettingsIcon } from "./accountSettings/AccountSettings.svg";
export { ReactComponent as BillingIcon } from "./accountSettings/Billing&Plans.svg";
export { ReactComponent as GlobalSolidIcon } from "./accountSettings/globe-solid.svg";
export { ReactComponent as InvoiceIcon } from "./accountSettings/invoice.svg";
export { ReactComponent as NotificationSIcon } from "./accountSettings/notificationS.svg";
export { ReactComponent as StaffIcon } from "./accountSettings/staff.svg";
export { ReactComponent as TaxIcon } from "./accountSettings/tax.svg";
export { ReactComponent as WebInfoIcon } from "./accountSettings/webInfo.svg";


export * from './design'