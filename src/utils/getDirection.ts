import i18n from 'i18next';

export const getDirection = () => {
  const currentLanguage = i18n.language;
  return currentLanguage === 'ar' ? 'rtl' : 'ltr';
}

